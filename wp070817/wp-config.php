<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_HOME','http://isimulate.com/');
define('WP_SITEURL','http://isimulate.com/');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iSimWInstall_070817');

/** MySQL database username */
// define('DB_USER', 'wedje');
define('DB_USER', 'root');
/** MySQL database password */
// define('DB_PASSWORD', 'y$v\A6Gh69\RuEk$');
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j{Zv4Ag@|(4xrIU;a,47fmW+J%FQ)DkA_P(%^=aF.J8?<M9kO,VUvhze70p?:8 k');
define('SECURE_AUTH_KEY',  '1+Gn~Rdm~W]25bv`lSc!_NLUbS2%]6h2={fC{pJX=leRY3 5ce~6X}wM*0E6R=RD');
define('LOGGED_IN_KEY',    '{@+OfK|+<QAr`Eb[S{,u0ac,DXeWS9|j~e%OE8u50XsCb$t.V,ObleEOnLq?a{P3');
define('NONCE_KEY',        'ZW09%Z1Lq&~Ldb=q- biF1UBl@DC{vb=UGkc9zbd?Gv>)Q|_#7[e2N9&3`7k_F2:');
define('AUTH_SALT',        'oZt72$#Wd-#1iE88)-nCy,IY?~Gs>#M,;2`#U`ihtH!gZP?>)I>^gs(I=l2sr&1Q');
define('SECURE_AUTH_SALT', '2eLK[5dL3EJK^:/UVaE6T`c!O1(c2^GIF8s~Tj]-Bfov10^9hK(ihYmr<3<&r216');
define('LOGGED_IN_SALT',   'f,A`62S-hOZikx70|f#sV?sw=::Y~Tj[Vim!_4kh)yK~RQlT}=S|m_wK6-ulH!$P');
define('NONCE_SALT',       'ae2P7zXrv;y{Q?<tO6&6@uKI~z4ES_e.E 7[gu5k%P0G4Xs&OI(][Gsek>W2]^-2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_POST_REVISIONS', 3);

/* Ran commentted this out on 01/08/2017, to recover the editor toolbar.
   If for safety reason need editor invisible, simply recover the code on line 89
*/
// define('DISALLOW_FILE_EDIT', true); //Don't allow to edit files from backend

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */

	if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="sidebar" class="blog-list-sidebar">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- .blog-list-sidebar -->
	<?php endif; ?>

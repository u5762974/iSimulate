/**
 * Public / Front Script
 *
 * Handles to control public / front script
 *
 * @since Higgs Fletcher & Mack 1.0
 **/
jQuery(document).ready(function($){
	
	/** Load More Your Story Index Posts **/
	$(document).on('click','.load-story',function(event){
		
		var $this 	= $(this);
		var technology = $('#technology').val(); //Fetch technology fiter value
		var att_articles = $this.attr('data-only-story');

		var ajaxdata= { action : 'isimulate_load_more_story', paged : $this.attr('data-paged') };
		//check technology set
		if( typeof technology != 'undefined' ){ ajaxdata['technology'] = technology; }//endif
		//AJAX Call
		$.ajax({
			url : Isimulate_Obj.ajaxurl,
			type: 'POST',
			data: ajaxdata,
			beforeSend: function(){
				$this.next('.ajax-loader').css('visibility','visible');
			},
			success: function(response){
				$this.next('.ajax-loader').css('visibility','hidden');
				var result = $.parseJSON(response);
				if( result.html ) {
					$(result.html).insertAfter($(".story-list-box").parent().find('.story-box').last());
					$this.attr('data-paged',result.paged);
					if( result.more_page == 0 ){
						$this.hide();
					}
				}
				$('.story-list-main-box .story-list-box .story-box .story-image:odd').addClass('odd');
				$('.story-list-main-box .story-list-box .story-box .stroy-details-box:odd').addClass('odd');
			}
		});
	});
	
		/** Filture Your Story Index Posts by country**/
	$(document).on('change','#country',function(event){

		var $this 	= $(this);
		var country = $('#country').val();
		var technology = $('#technology').val();
		var att_articles = $this.attr('data-only-attorney');
		var paged = '1';
		
		var ajaxdata= { action : 'isimulate_country_filter', paged : paged };

		//check country set
		if( typeof country != 'undefined' ){ ajaxdata['country'] = country; }//endif
		//check technology set
		if( typeof technology != 'undefined' ){ ajaxdata['technology'] = technology; }//endif
	
		//AJAX Call
		$.ajax({
			url : Isimulate_Obj.ajaxurl,
			type: 'POST',
			data: ajaxdata,
			beforeSend: function(){
				$this.next('.ajax-loader').css('visibility','visible');
			},
			success: function(response){
				$this.next('.ajax-loader').css('visibility','hidden');
				var result = $.parseJSON(response);
				$( ".story-list-box" ).html( result.html );
				if( result.html ) {
					$this.attr('data-paged',1);
					if( result.more_page == 0 ){
						$('.load-more-wrap').hide();
					}
				}
				$('.story-list-main-box .story-list-box .story-box .story-image:odd').addClass('odd');
				$('.story-list-main-box .story-list-box .story-box .stroy-details-box:odd').addClass('odd');
			}
		});
	});

	/** Filture Your Story Index Posts by technoplogy**/
	$(document).on('change','#technology',function(event){
		var $this 	= $(this);
		var country = $('#country').val();
		var technology = $('#technology').val();
		var att_articles = $this.attr('data-only-attorney');
		var paged = '1';
		
		var ajaxdata= { action : 'isimulate_technology_filter', paged : paged };
		
		//check country set
		if( typeof country != 'undefined' ){ ajaxdata['country'] = country; }//endif
		//check category set
		if( typeof technology != 'undefined' ){ ajaxdata['technology'] = technology; }//endif
	
		//AJAX Call
		$.ajax({
			url : Isimulate_Obj.ajaxurl,
			type: 'POST',
			data: ajaxdata,
			beforeSend: function(){
				$this.next('.ajax-loader').css('visibility','visible');
			},
			success: function(response){
				$this.next('.ajax-loader').css('visibility','hidden');
				var result = $.parseJSON(response);
//				$(".story-list-main-box").find('.story-list-box').remove();
				$( ".story-list-box" ).html( result.html );
//				$('.story-list-main-box').html(result.html);
				$('.story-list-main-box .story-list-box .story-box .story-image:odd').addClass('odd');
				$('.story-list-main-box .story-list-box .story-box .stroy-details-box:odd').addClass('odd');
				if( result.html ) {
					$this.attr('data-paged',1);
					if( result.more_page == 0 ){
						$('.load-more-wrap').hide();
					}
				}
			}
		});
	});
	
	/** adding odd even class to story box on your story page**/
	if( $('.story-list-main-box .story-list-box .story-box').length){
		$('.story-list-main-box .story-list-box .story-box .story-image:odd').addClass('odd');
		$('.story-list-main-box .story-list-box .story-box .stroy-details-box:odd').addClass('odd');
	}
	

});

/* Custom General jQuery
/*--------------------------------------------------------------------------------------------------------------------------------------*/
;(function($, window, document, undefined) {
	//Genaral Global variables
	var $win = $(window);
	var $doc = $(document);
	var $winW = function(){ return $(window).width(); };
	var $winH = function(){ return $(window).height(); };
	var $screensize = function(element){  
			$(element).width($winW()).height($winH());
		};
		
		var screencheck = function(mediasize){
			if (typeof window.matchMedia !== "undefined"){
				var screensize = window.matchMedia("(max-width:"+ mediasize+"px)");
				if( screensize.matches ) {
					return true;
				}else {
					return false;
				}
			} else { // for IE9 and lower browser
				if( $winW() <=  mediasize ) {
					return true;
				}else {
					return false;
				}
			}
		};

	$doc.ready(function() {
/*--------------------------------------------------------------------------------------------------------------------------------------*/		
		// Remove No-js Class
		$("html").removeClass('no-js').addClass('js');
		
		
		
		/* Get Screen size
		---------------------------------------------------------------------*/
		$win.load(function(){
			$win.on('resize', function(){
				$screensize('your selector');	
			}).resize();	
		});
		
		
		/* Menu ICon Append prepend for responsive
		---------------------------------------------------------------------*/
		$(window).on('resize', function(){
			if (screencheck(767)) {
				if(!$('#menu').length){
					$('#mainmenu').prepend('<a href="#" id="menu" class="menulines-button"><span class="menulines"></span> <em>Menu</em></a>');
				}
			} else {
				$("#menu").remove();
			}
		}).resize();

		/* This adds placeholder support to browsers that wouldn't otherwise support it. 
		---------------------------------------------------------------------*/
		if (document.createElement("input").placeholder === undefined) {
			var active = document.activeElement;
			$(':text').focus(function () {
				if ($(this).attr('placeholder') !== '' && $(this).val() === $(this).attr('placeholder')) {
					$(this).val('').removeClass('hasPlaceholder');
				}
			}).blur(function () {
				if ($(this).attr('placeholder') !== '' && ($(this).val() === '' || $(this).val() === $(this).attr('placeholder'))) {
					$(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
				}
			});
			$(':text').blur();
			$(active).focus();
			$('form:eq(0)').submit(function () {
				$(':text.hasPlaceholder').val('');
			});
		}
		
		
		/* Tab Content box 
		---------------------------------------------------------------------*/
		var tabBlockElement = $('.tab-data');
			$(tabBlockElement).each(function() {
				var $this = $(this),
					tabTrigger = $this.find(".tabnav li"),
					tabContent = $this.find(".tabcontent");
					var textval = [];
					tabTrigger.each(function() {
						textval.push( $(this).text() );
					});	
				$this.find(tabTrigger).first().addClass("active");
				$this.find(tabContent).first().show();

				
				$(tabTrigger).on('click',function () {
					$(tabTrigger).removeClass("active");
					$(this).addClass("active");
					$(tabContent).hide().removeClass('visible');
					var activeTab = $(this).find("a").attr("data-rel");
					$this.find('#' + activeTab).fadeIn('normal').addClass('visible');
								
					return false;
				});
			
				var responsivetabActive =  function(){
				if (screencheck(767)){
					if( !$('.tabMobiletrigger').length ){
						$(tabContent).each(function(index) {
							$(this).before("<h2 class='tabMobiletrigger'>"+textval[index]+"</h2>");	
							$this.find('.tabMobiletrigger:first').addClass("rotate");
						});
						$('.tabMobiletrigger').click('click', function(){
							var tabAcoordianData = $(this).next('.tabcontent');
							if($(tabAcoordianData).is(':visible') ){
								$(this).removeClass('rotate');
								$(tabAcoordianData).slideUp('normal');
								//return false;
							} else {
								$this.find('.tabMobiletrigger').removeClass('rotate');
								$(tabContent).slideUp('normal');
								$(this).addClass('rotate');
								$(tabAcoordianData).not(':animated').slideToggle('normal');
							}
							return false;
						});
					}
						
				} else {
					$('.tabMobiletrigger').remove();
					$this.find(tabTrigger).removeClass("active").first().addClass('active');
					$this.find(tabContent).hide().first().show();			
				}
			};
			$(window).on('resize', function(){
				if(!$this.hasClass('only-tab')){
					responsivetabActive();
				}
			}).resize();
		});
		
		/* Accordion box JS
		---------------------------------------------------------------------*/
		$('.accordion-databox').each(function() {
			var $accordion = $(this),
				$accordionTrigger = $accordion.find('.accordion-trigger'),
				$accordionDatabox = $accordion.find('.accordion-data');
				
				$accordionTrigger.first().addClass('open');
				$accordionDatabox.first().show();
				
				$accordionTrigger.on('click',function (e) {
					var $this = $(this);
					var $accordionData = $this.next('.accordion-data');
					if( $accordionData.is($accordionDatabox) &&  $accordionData.is(':visible') ){
						$this.removeClass('open');
						$accordionData.slideUp(400);
						e.preventDefault();
					} else {
						$accordionTrigger.removeClass('open');
						$this.addClass('open');
						$accordionDatabox.slideUp(400);
						$accordionData.slideDown(400);
					}
				});
		});
		
		/*Custom Radio and Checkbox
		---------------------------------------------------------------------*/
		if($('input[type="checkbox"], input[type="radio"]').length){
			$('input[type="checkbox"], input[type="radio"]').ezMark();
		}
		
		/* MatchHeight Js
		-------------------------------------------------------------------------*/
		if($('.benifits-box-details, .testimonials-main-box .testimonials-bg-image, .testimonials-main-box .testimonials-slider-box, .product-details-box, .product-feature-box-details, .pdf-download h5, .video-list-details-box h5, .distributors-address-box, .contact-address-box, .product-feature-details-box').length){
			$('.benifits-box-details, .testimonials-main-box .testimonials-bg-image, .testimonials-main-box .testimonials-slider-box, .product-details-box, .product-feature-box-details, .pdf-download h5, .video-list-details-box h5, .distributors-address-box, .contact-address-box, .product-feature-details-box').matchHeight();
		}
		if($('.heartisense-structure, .heartisense-features p, .heartisense-lms-box').length){
			$('.heartisense-structure, .heartisense-features p, .heartisense-lms-box').matchHeight();
		}
		
		if($('#gform_1 .gform_body > ul > li').length){
			$('#gform_1 .gform_body > ul > li').matchHeight();
		}
		
		if($('.product-list-image-box').length){
			$('.product-list-image-box').matchHeight({byRow:false});
		}
                
                if($('.product-list-details-box').length){
			$('.product-list-details-box').matchHeight({byRow:false});
		}
                
                
		$("#options > li > ul").parent('li').addClass('hasnav');
		$("#options > li.hasnav").prepend('<span class="navtrigger">+</span>');
		
		/*Mobile menu click
		---------------------------------------------------------------------*/
		$('.navtrigger').on('click tap', function(){
			$(this).toggleClass('menuopen');
			$(this).parent('li').find('ul').slideToggle('normal');
			return false;
		});
		
		$('#main-nav').on('click tap', function(event) {
			event.stopPropagation();
		});
		$('#menu-trigger').on('click tap', function(e){
			$(this).toggleClass('open-nav');
			$('#main-nav').toggleClass('openmenu');
		});
		$(document).on('click tap', function(e) {
			if( $(e.target).parent('#menu-trigger').length === 0 ) {
          		$('#main-nav').removeClass('openmenu');
          		$('#menu-trigger').removeClass('open-nav');
			}
		});
		
		//Smooth back to top
		$('.backtotop').click(function(){
			$("html:not(:animated),body:not(:animated)").animate({ scrollTop:0}, 'noraml');
			return false;
		});
		
		
		/* Get Screen size
		---------------------------------------------------------------------*/
		$win.load(function(){
			$win.on('resize', function(){
				$screensize('.hero-slider .slide-item');	
			}).resize();	
		});
		
		/* Hero-slider
		---------------------------------------------------------------------*/
		if( $('.hero-slider').length){
			var heroSlider = $('.hero-slider');
			heroSlider.owlCarousel({
				loop:true,
				margin:0,
				nav:true,
				dots:true,
				smartSpeed: 370,
				items:1,
				//dotData:true,
				responsive:{
					768:{
						items:1
					}
				}
			});
			heroSlider.find('.owl-controls').append('<span class="counter"><span>'+'0'+(heroSlider.find('.owl-dots .owl-dot.active').index() + 1)+'</span><em>/0'+heroSlider.find('.owl-dots .owl-dot').size()+'</em></span>');
			
			heroSlider.on('translated.owl.carousel', function(event) {
				$('.grid-lines').addClass('animate');
				var currentIndex = heroSlider.find('.owl-dots .owl-dot.active').index()+1;
				heroSlider.find('.owl-controls .counter > span').html('0'+currentIndex);
			});
		}
		
		
		/* testiminials-slider
		---------------------------------------------------------------------*/
		if( $('.testimonial-slider').length){
			var testiSlider = $('.testimonial-slider');
			testiSlider.owlCarousel({
				loop:true,
				margin:0,
				nav:true,
				dots:false,
				smartSpeed: 370,
				items:1,
				responsive:{
					768:{
						items:1
					}
				}
			});
		}		
		
		/* Smooth Scroll
		---------------------------------------------------------------------*/
		$(function() {
		  $('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html, body').animate({
				  scrollTop: target.offset().top - 50
				}, 1000);
				return false;
			  }
			}
		  });
		});
		
		/*Lightbox
		-------------------------------------------------------------------------*/
		if($('.venoboxvid').length){
			 $('.venoboxvid').venobox({
				bgcolor: '#000'
			});
		}
		
		/* Main Navigation Sticky
		 * ----------------------------------------------------------------------------------------------------------------------*/
		//var intialtop = $(document).scrollTop();
		var headerheight = $("#header").outerHeight();
		$(window).scroll(function() {
			/*var afterscrolltop = $(document).scrollTop();
			if( afterscrolltop > headerheight ) {
				$("#header").addClass("navhide");
			} else {
				$("#header").removeClass("navhide");
			}
			if( afterscrolltop > intialtop ) {
				$("#header").removeClass("navshow");
			} else {
				$("#header").addClass("navshow");
			}
			intialtop = $(document).scrollTop();*/
			if( $(this).scrollTop() > 2 ) {
			  $("#header").addClass("navhide");
			} else {
			  $("#header").removeClass("navhide");
			}
		});
		
		/* image to BG 
		--------------------------------------------------------------------------------------------------------------------------------------*/
		$('.testimonials-bg-image img').each(function(index, element) {
				var imgSrc = $(this).attr('src');
				$(this).parent('.testimonials-bg-image').css('background-image', 'url('+imgSrc+')')/*.height(imgHeight)*/;
		});	
		
		/* Popup box
		--------------------------------------------------------------------------------------------------------------------------------------*/
		var $dialogTrigger = $('.poptrigger'),
		$pagebody =  $('body');
		$dialogTrigger.click( function(){
			var popID = $(this).attr('data-rel');
			$('body').addClass('overflowhidden');
			var winHeight = $(window).height();
			$('#' + popID).fadeIn();
			var popheight = $('#' + popID).find('.popup-block').outerHeight(true);
			
			if( $('.popup-block').length){
				var popMargTop = popheight / 2;
				//var popMargLeft = ($('#' + popID).find('.popup-block').width()/2);
				
				if ( winHeight > popheight ) {
					$('#' + popID).find('.popup-block').css({
						'margin-top' : -popMargTop,
						//'margin-left' : -popMargLeft
					});	
				} else {
					$('#' + popID).find('.popup-block').css({
						'top' : 0,
						//'margin-left' : -popMargLeft
					});
				}
				
			}
			$('#' + popID).append("<div class='modal-backdrop'></div>");
			$('.popouterbox .modal-backdrop').fadeTo("slow", 0.80);
			if( popheight > winHeight ){
				$('.popouterbox .modal-backdrop').height(popheight);
			} 
			$('#' + popID).focus();
			return false;
		});
		/* Popup Box
		--------------------------------------------------------------------------------------------------------------------------------------*/
		$(window).on("resize", function () {
			if( $('.popouterbox').length && $('.popouterbox').is(':visible')){
				var popheighton = $('.popouterbox .popup-block').height()+60;
				var winHeight = $(window).height();
				if( popheighton > winHeight ){
					$('.popouterbox .modal-backdrop').height(popheighton);
					$('.popouterbox .popup-block').removeAttr('style').addClass('taller');
					
				} else {
					$('.popouterbox .modal-backdrop').height('100%');
					$('.popouterbox .popup-block').removeClass('taller');
					$('.popouterbox .popup-block').css({
						'margin-top' : -(popheighton/2)
					});
				}	
			}
		});
		
		//Close popup		
		$(document).on('click', '.close-dialogbox, .modal-backdrop', function(){
			$(this).parents('.popouterbox').fadeOut(300, function(){
				$(this).find('.modal-backdrop').fadeOut(250, function(){
					$('body').removeClass('overflowhidden');
					$('.popouterbox .popup-block').removeAttr('style');
					$(this).remove();
				});
			});
			return false;
		});
		
		/* testiminials-slider
		---------------------------------------------------------------------*/
		if( $('.heartisense-slider').length){
			var testiSlider = $('.heartisense-slider');
			testiSlider.owlCarousel({
				loop:true,
				margin:0,
				nav:true,
				dots:false,
				smartSpeed: 370,
				items:1,
				responsive:{
					768:{
						items:1
					}
				}
			});
		}
		
		/*search text hover effect
		---------------------------------------------------------------------*/
		$('.searchtxt input[type="submit"]').hover(function(){
			$(this).parents('.searchtxt').addClass('hover');
		}, function(){
			$(this).parents('.searchtxt').removeClass('hover');
		});
		
		/*GOOGLEMAPS TO CONTACT PAGE 
	===========================================================================*/
	if($('#contact-map').length){
				var styles = [{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"},{"color":"#f4f6f7"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"off"},{"color":"#dee2e4"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry","stylers":[{"lightness":-25},{"saturation":-97},{"color":"#a4afb6"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]}];
	
				

				var locations = [];
				var icons = [];
				/* retrive office title, lng,lat detail */
				for ( var i = 0; i < Isimulate_Genrs.office_address.title.length; i++) {
					locations.push([Isimulate_Genrs.office_address.title[i] , Isimulate_Genrs.office_address.lat[i], Isimulate_Genrs.office_address.lng[i]]);
					icons.push(Isimulate_Genrs.imgurl + "/marker-pin.png");
				}
				/* retrive destributor title, lng,lat detail */
				for ( var j = 0; j < Isimulate_Genrs.distributor_address.title.length; j++) {
					locations.push([Isimulate_Genrs.distributor_address.title[j], Isimulate_Genrs.distributor_address.lat[j], Isimulate_Genrs.distributor_address.lng[j]]);
					icons.push(Isimulate_Genrs.imgurl + "/marker-dis.png");
				}

				

				var map = new google.maps.Map(document.getElementById('contact-map'), {
					zoom: 3,
					center: new google.maps.LatLng(20.1024814, 76.6676495),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					scrollwheel: true,
					panControl: false,
					mapTypeControl:false,
					streetViewControl: false,
					disableDefaultUI: false,
					zoomControl: true,
                                        minZoom: 3, 
                                        maxZoom: 15,
					styles: styles
				});
			
				var infowindow = new google.maps.InfoWindow();
			
				var marker, m;
				var iconCounter =0;
				
				for (m = 0; m < locations.length; m++) { 
				  marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[m][1], locations[m][2]),
					map: map,
					icon: icons[iconCounter]
				  });
				  iconCounter++;
				  var infotitle ='';
				  google.maps.event.addListener(marker, 'click', (function(marker, m) {
					return function() {
					  infotitle = '<strong>' + locations[m][0] + '</strong>';
					  infowindow.setContent(infotitle);
					  infowindow.open(map, marker);
					}
				  })(marker, m));
				}


	}
	if($(".knob").length){
		$(".knob").knob();
	}
/*--------------------------------------------------------------------------------------------------------------------------------------*/		
	});	
/*--------------------------------------------------------------------------------------------------------------------------------------*/
})(jQuery, window, document);
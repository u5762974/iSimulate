/**
 * Admin Script
 *
 * Handles to control admin script
 *
 * @since Higgs Fletcher & Mack 1.0
 **/
jQuery(document).ready(function($){
	
	//check element exist or not
	if( $('#acf-field-practice_groups, #acf-field-related_practice_area, #acf-field-news_posts_categories, .chosen-select, .chosen-select-widget').length > 0 ){
		$('#acf-field-practice_groups, #acf-field-related_practice_area, #acf-field-news_posts_categories, .chosen-select, .chosen-select-widget').chosen({'width' : '100%'});
	}
	
	/** Add More PDF Brochure **/
	$(document).on('click', '.add-more-brochure', function() {
		var parents = $(this).parents('.widget-content');
		var brouchurecount = parents.find('.widget-pdf-brochure').length;
		var newmarkup = parents.find('.widget-pdf-brochure:first').clone();
		newmarkup.html(function(i, oldhtml ){
			return oldhtml.replace(/\[0\]/g, '['+brouchurecount+']');
		});
		//replace title of block
		newmarkup.find('.brochure-number-title').html('Brochure ' + ( brouchurecount + 1 ));
		//empty all new inputs
		newmarkup.find('input:text, textarea').val('');
		$('<input type="button" class="button-secondary delete-brochure" value="Delete"/>').insertBefore( newmarkup.find('.brochure-number-title') );
		//append after
		$(newmarkup).insertAfter( parents.find('.widget-pdf-brochure:last') );
	});
	/** Remove PDF Brochure **/
	$(document).on('click', '.delete-brochure', function() {
		//delete clicked brochure
		$(this).parents('.widget-pdf-brochure').remove();
	});
	
	/** Show Bios Content For Practice Groups Selected **/
	var showbioscontent = function( selectbox ){
		selectbox.find('option').each(function(){
			var selecteditem = $(this).val();
			var showthis = $('div#acf-att_bios_'+selecteditem+'*');
			if( $(this).is(':selected') ) {
				$('div[id^=acf-att_bios_'+selecteditem+']').show();
			} else {
				$('div[id^=acf-att_bios_'+selecteditem+']').hide();
			}
		});
	};
	
	/** Practice Groups With In ACF **/
	if( $('#acf-field-practice_groups').length ) {
		showbioscontent($('#acf-field-practice_groups'));
	}//endif
	
	/** Show Bios Content For Practice Groups Selected **/ 
	$(document).on('change', '#acf-field-practice_groups', function() {
		showbioscontent( $(this) );
	});
	//Call When Widget Updated
	$(document).on('widget-updated widget-added', function(event, widget){
		if( $('.chosen-select-widget').length ) {
			$('.chosen-select-widget').chosen({'width' : '100%'});
		}
	});
});
<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>

    <div class="blog-details-main-box">
        <div class="blog-list-item-details-box">
            <?php the_title('<h1>','</h1>'); ?>
            <?php isimulate_entry_meta();  //entry meta ?>
            <div class="blog-details-box">
                <?php if( has_post_thumbnail() ) : //Check Has Post Thumbnail?>
                    <figure>
                        <?php the_post_thumbnail('full', array( 'alt' => get_the_title() ) );?>
                    </figure>
                <?php endif;?>
                <?php the_content(); ?>
           	</div>
        	<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;?>
        
        </div><!--/.blog-list-item-details-box-->
    </div><!--/.blog-list-item-box-->
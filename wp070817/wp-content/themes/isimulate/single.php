<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */

get_header(); ?>

	<div class="wrap">
		<div id="primary" class="content-area blog-page">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', get_post_format() );

		// End the loop.
		endwhile;
		?>
		</div><!-- #primary -->
        <?php get_sidebar();?>
	</div><!-- .wrap -->

<?php get_footer(); ?>

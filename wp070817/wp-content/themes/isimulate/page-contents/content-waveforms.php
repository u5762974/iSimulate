<?php
/**
 * The template for displaying waveforms page
 *
 * This is the template that displays waveforms page content
 *
 * @since iSimulate 1.0
 **/
 	if( get_field('page_title') || get_field('page_title') || get_the_content() ):?>
        <div class="waveforms-main-title">
            <div class="wrap">
                <?php if( get_field('page_title') || get_field( 'page_sub_title' )):?>
                    <h2 class="section-title">
                        <?php if( get_field('page_title') ): // To check page title?>
                        <span><?php the_field('page_title'); ?></span>
                        <?php endif;
                        if( get_field('page_sub_title') ): //to check page sub title
                            the_field('page_sub_title');
                        endif;?>
                    </h2>
                <?php endif;
				if( get_the_content() ): ?>
                    <div class="product-box">
                        <?php the_content(); //To retrive content?>
                    </div>
                <?php endif; ?>
            </div><!--/.wrap -->
        </div><!--/.download-main-box -->
    <?php endif; 
	
	if( have_rows('waveforms') ): //To check wave forms data?>

        <div class="waveforms-main-box">

           <?php while ( have_rows('waveforms') ) : the_row(); //start loop for waveforms
					$even_odd = $even_odd == 'even' ? 'odd' : 'even'; ?>
                    <div class="waveforms-box-list <?php echo $even_odd;?>">
                        <div class="wrap">
        
                            <?php if( get_sub_field('title') ): //Waveforms title?>
                                <h3><?php the_sub_field('title'); ?></h3>
                            <?php endif;
        
                            if( have_rows('waveforms_details') ): //To check wave forms data?>
                                <div class="waveforms-box-table">
                                    <table class="res-table">
                                        <tbody>
                                            <tr>
                                                <th><?php _e('No.','isimulate');?></th>
                                                <th><?php _e('Sinus','isimulate');?></th>
                                                <th><?php _e('Heart rate Range','isimulate');?></th>
                                                <th><?php _e('Default Heart Rate','isimulate');?></th>
                                            </tr>
                                            <?php $counter = 1; //Counter for table serial no.
                                            while ( have_rows('waveforms_details') ) : the_row(); //start loop for waveforms?>
                                                <tr>
                                                    <td data-th="<?php _e('No.','isimulate');?>"><?php echo $counter++;?></td>
                                                    <td data-th="<?php _e('Sinus','isimulate');?>"><?php the_sub_field('sinus');?></td>
                                                    <td data-th="<?php _e('Heart rate Range','isimulate');?>">0 – 258</td>
                                                    <td data-th="<?php _e('Default Heart Rate','isimulate');?>">78</td>
                                                </tr>
                                            <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                </div><!--/.waveforms-box-table-->
        
                                <?php if($waveforms_image = get_sub_field('image')):
                                        $size = 'waveforms-thumb'; ?>
                                    <div class="waveforms-box-image">
                                        <a class="venoboxvid vbox-item" href="<?php echo $waveforms_image['url'];?>"><img src="<?php echo $waveforms_image['sizes'][$size];?>" alt="waveforms"></a>
                                    </div><!--/.waveforms-box-image-->
                                <?php endif;
                            endif; ?>
        
                        </div><!--/.wrap-->
                    </div><!--/.waveforms-box-list-->

					<?php if( have_rows('sub_waveforms') ): //To check sub waveforms data
                            while ( have_rows('sub_waveforms') ) : the_row(); //start loop for waveforms
                                $even_odd = $even_odd == 'even' ? 'odd' : 'even'; //Even Odd Class?>
                                <div class="waveforms-box-list <?php echo $even_odd; ?>">
                                    <div class="wrap">
                                        <?php if( get_sub_field('title') ): //Waveforms title?>
                                            <h5><?php the_sub_field('title'); ?></h5>
                                        <?php endif;
										if( have_rows('sub_waveforms_details') ): //To check wave forms data?>
                                            <div class="waveforms-box-table">
                                                <table class="res-table">
                                                    <tbody>
                                                        <tr>
                                                            <th><?php _e('No.','isimulate');?></th>
                                                            <th><?php _e('Sinus','isimulate');?></th>
                                                            <th><?php _e('Heart rate Range','isimulate');?></th>
                                                            <th><?php _e('Default Heart Rate','isimulate');?></th>
                                                        </tr>
                                                        <?php	$counter =1; //Counter for table serial no.
														while ( have_rows('sub_waveforms_details') ) : the_row(); //start loop for waveforms?>
                                                            <tr>
                                                                <td data-th="<?php _e('No.','isimulate');?>"><?php echo $counter++;?></td>
                                                                <td data-th="<?php _e('Sinus','isimulate');?>"><?php the_sub_field('sinus');?></td>
                                                                <td data-th="<?php _e('Heart rate Range','isimulate');?>">0 – 258</td>
                                                                <td data-th="<?php _e('Default Heart Rate','isimulate');?>">78</td>
                                                            </tr>
                                                        <?php endwhile; ?>
                                                    </tbody>
                                                </table>
                                            </div><!--/.waveforms-box-table-->
                    
                                            <?php if($sub_waveforms_image = get_sub_field('image')):
                                                    $size = 'waveforms-thumb'; ?>
                                                <div class="waveforms-box-image">
                                                    <a class="venoboxvid vbox-item" href="<?php echo $sub_waveforms_image['url'];?>"><img src="<?php echo $sub_waveforms_image['sizes'][$size];?>" alt="waveforms"></a>
                                                </div><!--/.waveforms-box-image-->
                                            <?php endif;
                                        endif; ?>
                                    </div><!--/.wrap-->
                                </div><!--/.waveforms-box-list-->
                            <?php endwhile;
                        endif;
			endwhile; //End loop for waveforms?>
		</div><!--/.waveforms-main-box-->
    <?php endif; ?>
            
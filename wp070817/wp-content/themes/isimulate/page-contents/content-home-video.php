<?php
/**
 * The template for displaying video section
 *
 * This is the template that displays video section
 *
 * @since iSimulate 1.0
 **/
 	if( get_field('video_section_image_banner') ): 
	$background =( $image = get_field( 'video_section_image_banner' ))? ' style= "background-image: url('.get_field( 'video_section_image_banner' ).')"' :'';
	$videourl = ( get_field( 'video_section_video_url' ) ) ? get_field( 'video_section_video_url' ) : '#';
	$venobox = ( get_field( 'video_section_video_url' ) ) ? ' venoboxvid vbox-item' : ''; // to check video for venobox ?> 
        <div class="video-main-box"<?php echo $background; ?>>
            <a href="<?php echo $videourl; ?>" class="video-box <?php echo $venobox; ?>" data-type="youtube">
    
                <?php if( get_field( 'video_section_title' ) ): ?>
                    <h3><?php the_field( 'video_section_title' ); ?></h3>
                <?php endif;
    
                if($venobox): ?>
                    <div class="video"><?php _e('Video'); ?></div>
                <?php endif; ?>
            </a><!--/.video-box -->
        </div><!--/.video-main-box -->
	<?php endif;?>
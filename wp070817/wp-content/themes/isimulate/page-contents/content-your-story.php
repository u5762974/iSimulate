<?php
/**
 * The template for displaying home page
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/
 	$your_story_post = new WP_Query( array( 'post_type' => 'your-story', 'published' => true));
	$posts_args['tax_query'] = array(array('taxonomy'=>'group','field'=>'slug','terms'=> $_POST['technology'])); 
	if( $your_story_post->have_posts()): //check post in your story custom post?>
        <div class="filer-section-box">
            <div class="wrap">
                <div class="filter-box">
                    <h5><?php _e('Filter By'); ?></h5>	
                    <div class="form-block form-inline fliter-select">
                        <?php $terms_technology = get_terms( array( 'taxonomy' => 'group', 'hide_empty' => false ) ); //To retrive technology list in dropdown 
                        if($terms_technology):?>
                            <div class="form-group">
                                <div class="field-box">
                                    <div class="custom-select">
                                        <select name="technology" id="technology">
                                            <option value=''><?php _e('All Group');?></option>
                                            <?php foreach($terms_technology as $term_technology):?>
                                                <option value="<?php echo $term_technology->slug;?>"><?php echo $term_technology->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div><!--/.field-box-->	
                            </div>
                        <?php endif; ?>
                    
                        <?php $terms_country = get_terms( array( 'taxonomy' => 'country', 'hide_empty' => false ) ); //To retrive country list in dropdown 
                        if($terms_country):?>
                            <div class="form-group">
                                <div class="field-box">
                                    <div class="custom-select">
                                        <select name="country" id="country">
                                            <option value=""><?php _e('Country');?></option>
                                            <?php foreach($terms_country as $term_country):?>
                                               <option value="<?php echo $term_country->slug; ?>"><?php echo $term_country->name;?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div><!--/.field-box-->	
                            </div>
                        <?php endif; ?>
                    </div><!--/.form-block-->
                </div>
                <div class="write-story-box">
                    <a data-rel="edit-story" class="poptrigger button btn-lg write-story" href="#">Write Your Story</a>
                </div><!--/.write-story-box-->
            </div><!--/.wrap-->
        </div>
		<div class="story-list-main-box">
            <div class="wrap">
                <div class="story-list-box">
                	<?php while( $your_story_post->have_posts() ) : $your_story_post->the_post(); //Loop start
						//request a quote section
						get_template_part( 'page-contents/content', 'your-story-box' );
                    endwhile;?>
                    <?php isimulate_load_more_button($your_story_post);?>
                </div><!--/.story-list-box-->
            </div><!--/.wrap-->
        </div><!--/.story-list-main-box-->
    <?php
	else:
		get_template_part( 'content', 'none' );
	endif;
	//request a quote section
    get_template_part( 'page-contents/content', 'testimonials' );
	//request a quote section
    get_template_part( 'page-contents/content', 'request-quote' );?>
<?php
/**
 * The template for displaying blog listing
 *
 * This is the template that displays blog listing
 *
 * @since iSimulate 1.0
 **/
?>
<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>
    <div class="blog-list-item-box">
        <div class="post-item-date">
        	<?php $date = get_the_date();?>
            <span><?php echo date('d',strtotime($date));?></span>
            <span><?php echo date('M',strtotime($date));?></span>
        </div><!--/.post-item-date-->
        <div class="blog-list-item-details-box">
        	<h4><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h4>
            <?php isimulate_entry_meta();  //entry meta ?>
            <div class="blog-post-image-block">
                <?php if( has_post_thumbnail() ) : //Check Has Post Thumbnail?>
	                <figure>
						<?php the_post_thumbnail('post-thumb',array( 'alt' => get_the_title() ) );?>
                    </figure>
                <?php endif; ?>
                <?php if(get_the_content()):?>
					<p><?php echo wp_trim_words( get_the_content(),45 ); //Get content for news post ?></p>
                <?php endif; ?>
                <a class="button post-read" href="<?php echo get_permalink();?>"><?php _e('Read more','isimulate');?></a>
            </div><!--/blog-post-image-block.-->
        </div><!--/.blog-list-item-details-box-->
    </div><!--/.blog-list-item-box-->
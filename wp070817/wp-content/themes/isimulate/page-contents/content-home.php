<?php
/**
 * The template for displaying home page
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/
 
	//Our product section
	get_template_part( 'page-contents/content', 'our-product' );
	
	//general product features
	get_template_part( 'page-contents/content', 'home-product-feature' );
	
	//Our history section
	get_template_part( 'page-contents/content', 'our-history' );
	
	//home page video section
	get_template_part( 'page-contents/content', 'home-video' );
	
	//home page testimonials
	get_template_part( 'page-contents/content', 'testimonials' );
	
	//home page request a quote
	get_template_part( 'page-contents/content', 'request-quote' );
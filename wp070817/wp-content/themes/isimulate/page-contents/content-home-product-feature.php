<?php
/**
 * The template for displaying home product feature
 *
 * This is the template that displays home product feature
 *
 * @since iSimulate 1.0
 **/ 
	if( have_rows( 'home_product_features' ) ): //To check feature ?>
		<div class="benifits-main-box" style="background-image:url(<?php echo ISIMULATE_THEME_URL;?>/images/bgi/benifits.jpg)">
         	<div class="cols cols3">
			<?php while( have_rows( 'home_product_features' ) ) : the_row(); //Loop Start ?>
				<div class="col">
                    <div class="benifits-box-details">
                    	<?php if( get_sub_field( 'title' ) || get_sub_field('description') ):?>
                        <h2><?php echo $num_padded = sprintf("%02d", ++$counter);?></h2>
                        <div class="benifits-details">
                        	<?php if( get_sub_field('title') ): ?>
                            	<h5><?php the_sub_field('title'); ?></h5>
                            <?php endif;
							if( get_sub_field('description') ):?>
	                            <p><?php the_sub_field('description'); ?></p>
                            <?php endif; ?>
                        </div>
                        <?php endif;?>
                    </div><!--/.benifits-box-details -->
                </div>
			<?php endwhile; ?>
      		</div>
       	</div>
	<?php endif; ?>
<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
	<div class="download-main-box">
        <div class="wrap">
            <?php if( get_field( 'page_title' ) || get_field( 'page_sub_title' ) ):?>
                <h2 class="section-title">
                    <?php if( get_field( 'page_title' ) ): ?>
                        <span><?php the_field( 'page_title' ); ?></span>
                    <?php endif;
                    if( get_field( 'page_sub_title' ) ):
                        the_field( 'page_sub_title' );
                    endif;?>
                </h2>
            <?php endif; ?>
            <div class="product-box">
                <?php the_content(); ?>
            </div>
        </div><!--/.wrap -->
        <?php if( have_rows( 'video_details' ) ): //To check product highlights ?>
            <div class="video-list-main-box">
                <div class="wrap">
                    <div class="video-list-box">
                        <div class="cols cols4">
                            <?php while( have_rows( 'video_details' ) ) : the_row(); //Loop Start
								if( get_sub_field( 'youtube_link' ) ):?>
                                <div class="col">
                                    <div class="video-list-details-box">
                                        <a class="venoboxvid vbox-item" data-type="youtube" href="<?php echo get_sub_field( 'youtube_link' ); ?>" data-gall="gall-video">
                                            <div class="video-list-image-box">
                                            	<?php if( $video_thumb = get_sub_field( 'thumbnail' ) ): ?>
	                                                <img alt="<?php echo get_sub_field( 'title' ); ?>" src="<?php echo $video_thumb['sizes']['video-thumb'];?>">
                                                <?php else:?>
    	                                            <img alt="<?php echo get_sub_field( 'title' ); ?>" src="<?php echo ISIMULATE_DEFAULT_VIDEO_THUMB; ?>">
                                                <?php endif; ?>
                                            </div><!--/.product-list-image-box -->
                                            <?php if( get_sub_field('title') ):?>
	                                            <h5><?php the_sub_field('title'); ?></h5>
                                            <?php endif; ?>
                                        </a>
                                    </div><!--/.product-list-details-box-->
                                </div><!--/.col-->
                            <?php endif;
							endwhile; ?>
                        </div>
                    </div><!--/.video-list-box-->    
                </div><!--/.wrap-->
            </div><!--/.download-main-box-->
        
        <?php endif; ?>
  	</div><!--/.download-main-box -->
	<?php //request a quote section
    get_template_part( 'page-contents/content', 'request-quote' );?>
	
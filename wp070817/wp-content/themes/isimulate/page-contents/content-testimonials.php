<?php
/**
 * The template for displaying testimonials
 *
 * This is the template that displays testimonials
 *
 * @since iSimulate 1.0
 **/
 	if( get_field('testimonials','option' ) ): // To check testimonials?>
    
        <div class="testimonials-main-box">
            <div class="testimonials-slider-box">
                <div class="testimonials-details">
                	<?php if( get_field( 'testimonial_title','option' ) || get_field( 'testimonial_sub_title','option' ) ): ?>
	                    <h2 class="section-title white">
                        	<?php if( get_field('testimonial_title','option' ) ): //To retrive title ?>
	                        	<span><?php the_field( 'testimonial_title','option' ); ?></span>
                            <?php endif;
							if( get_field('testimonial_sub_title','option') ): // To retrive sub title
								the_field('testimonial_sub_title','option' );
                           	endif;
							?>
                        </h2>
                        
                    <?php endif;

					if( get_field( 'testimonials','option' ) ): ?>
                        <div class="testimonial-slider">
                        	<?php while( have_rows( 'testimonials','option' ) ) : the_row(); //Loop Start ?>
                                <div class="slide-item">
                                    <div class="testimonials-details-box">
                                    <?php if( get_sub_field( 'description','option' ) ): //Testimonial description ?>
                                        <p><?php the_sub_field( 'description','option' ); ?></p>
                                    <?php endif;
    
                                    if( get_sub_field( 'name','option' ) ): //to check name ?>
                                        <h3><?php the_sub_field( 'name','option' ); ?></h3>
                                    <?php endif;
    
                                    if( get_sub_field( 'designation','option' ) ):  // To check designation ?>
                                        <span class="testimonials-role"><?php the_sub_field( 'designation','option' );  ?></span>
                                    <?php endif; ?>
                                    </div><!--/.testimonials-details-box -->
                                </div><!--/.slide-item -->
                            <?php endwhile; ?>
                        </div><!--/.testimonial-slider -->
                    <?php endif; ?>
                </div><!--/.testimonials-details -->
            </div><!--/.testimonials-slider-box -->
            <?php if( $testimonials_image = get_field('testimonial_image','option' ) ): //To check testimonial image ?>
                <div class="testimonials-bg-image">
                    <img src="<?php echo $testimonials_image['url'];?>" alt="<?php bloginfo();?>">
                </div><!--/.testimonials-bg-image -->
            <?php endif; ?>
        </div><!--/.testimonials-main-box -->
    <?php endif; ?>
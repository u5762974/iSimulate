<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
    <div class="our-product-box">
        <div class="wrap">
        	<?php if( get_field('page_title') || get_field( 'page_sub_title' )):?>
	            <h2 class="section-title">
                	<?php if( get_field('page_title') ): ?>
                	<span><?php the_field('page_title');?></span>
                    <?php endif;
					if( get_field('page_sub_title') ):
						the_field('page_sub_title');
					endif;?>
               	</h2>
            <?php endif; ?>

            <div class="product-box">
                <?php the_content();
				if( get_field('login_button_link') ):
					$login_button_text = (get_field('login_button_link'))? get_field('login_button_text') : 'iSimulate Online Login'; ?>
	                <a class="button btn-lg request-btn" href="<?php the_field('login_button_link');?>"><?php echo $login_button_text; ?></a>
                <?php endif; ?>
            </div>
        </div><!--/.wrap -->
    </div><!--/.history-main-box -->

    <?php if( have_rows( 'main_feature_details' ) ): //To check main features ?>
        <div class="isimulate-community-box">
            <div class="wrap">
                <div class="product-details-list">	
                    <div class="cols cols3">
                    	<?php while( have_rows( 'main_feature_details' ) ) : the_row(); //Loop Start ?>
                            <div class="col">
                                <div class="product-details-box">
                                    <div class="product-image-box">
                                    	<?php if( $feature_image = get_sub_field('image')):?>
                                        	<img src="<?php echo $feature_image['url'];?>" alt="<?php the_sub_field('title');?>">
                                        <?php else: ?>
											<img src="<?php echo ISIMULATE_DEFAULT_PRODUCT_THUMB; ?>" alt="<?php the_title();?>">
										<?php endif;?>
                                    </div><!--/.product-image-box -->
                                    <?php if( get_sub_field( 'title' ) ): ?>
	                                    <h3><?php the_sub_field('title'); ?></h3>
                                    <?php endif; ?>
                                </div><!--/.product-details-box-->
                            </div><!--/.col-->
                        <?php endwhile;?>
                    </div>
                </div><!--/.product-details-list-->
            </div><!--/.wrap-->
        </div><!--/.isimulate-community-box-->
    <?php endif; ?>
	
    <?php if( get_field( 'other_feature_details' ) ): //To check main features ?>
        <div class="waveforms-list-box">
            <div class="wrap">
                <div class="waveforms-box list-block">
                	<?php the_field('other_feature_details');?>
                </div><!--/.waveforms-box-->
            </div><!--/.wrap-->
        </div><!--/.waveforms-list-box-->
    <?php endif; ?>
    
    <div class="faq-download-main-box">
        <div class="wrap">
            <div class="cols cols2">
            	<?php if( get_field( 'download_section_title' ) || get_field( 'download_section_description' ) ): // to check download section detail ?>
                    <div class="col">
                        <div class="download-information-box download">
                            <div class="faq-download-details-box">
                            	<?php if( get_field('download_section_title') ): //To check section title ?>
	                                <h3><?php the_field('download_section_title'); ?></h3>
                                <?php endif;
								if( get_field('download_section_description') ): // to check section description ?>
	                                <p><?php the_field('download_section_description'); ?></p>
								<?php endif;
								if( get_field('download_section_button_link') )://To check button link 
									$download_button_text = get_field('download_section_button_text')? get_field('download_section_button_text') : 'Download'; ?>
	                                <a href="<?php the_field('download_section_button_link'); ?>" class="button btn-lg"><?php echo $download_button_text; ?></a>
                                <?php endif; ?>
                            </div><!--/.faq-download-details-box-->
                        </div><!--/.download-information-box download-->
                    </div><!--/.col -->
                <?php endif;

				if( get_field( 'faq_section_title' ) || get_field('faq_section_description') ): //To check faq section detail ?>
					<div class="col">
						<div class="download-information-box faq">
							<div class="faq-download-details-box">
                            	<?php if( get_field('faq_section_title') ): //To check section title?>
									<h3><?php the_field('faq_section_title'); ?></h3>
                                    <?php endif;
                                if( get_field('faq_section_description') ): // to check section description ?>
	                                <p><?php the_field('faq_section_description'); ?></p>
								<?php endif;
								if( get_field('faq_section_button_link') )://To check button link 
									$faq_button_text = get_field('faq_section_button_text')? get_field('faq_section_button_text') : 'GO TO FAQ'; ?>
	                                <a href="<?php the_field('faq_section_button_link'); ?>" class="button btn-lg"><?php echo $faq_button_text; ?></a>
                                <?php endif; ?>
							</div><!--/.faq-download-details-box-->
						</div><!--/.download-information-box download-->
					</div><!--/.col -->
				<?php endif; ?>
            </div>
        </div><!--/.wrap-->
    </div>
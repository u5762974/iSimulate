<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>

    <div class="blog-details-main-box">
        <div class="blog-list-item-details-box">
            <div class="blog-details-box">
                <?php the_content(); ?>
           	</div>
        </div><!--/.blog-list-item-details-box-->
    </div><!--/.blog-list-item-box-->
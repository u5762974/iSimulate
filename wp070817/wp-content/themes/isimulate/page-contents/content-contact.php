<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
    <div class="download-main-box">
        <div class="wrap">
        	<?php if( get_field( 'page_title' ) || get_field( 'page_sub_title' ) ):?>
                <h2 class="section-title">
                    <?php if( get_field('page_title') ):?>
                    <span><?php the_field( 'page_title' ); ?></span>
                    <?php endif;
                    if( get_field( 'page_sub_title' ) ):
                        the_field( 'page_sub_title' );
                    endif;?>
                </h2>
            <?php endif; ?>
			
            <div class="contact-main-box">
            	<?php if( have_rows( 'office_details' ) ): ?>
	            <div class="cols cols3">
                        <?php while( have_rows( 'office_details' ) ) : the_row(); ?>
                        <div class="col">
                            <?php $background_image = get_sub_field('contact_background_image');
                            $image = ($background_image) ? $background_image['url']: ISIMULATE_THEME_URL.'/images/bgi/office-newzealand.png'; ?>
                            <div class="contact-address-box office-australia" style="background-image:url(<?php echo $image; ?>);">
                            	<?php if( get_sub_field('title') ): ?>
	                                <h4><?php the_sub_field('title'); ?></h4>
                                <?php endif;?>
				<ul>
                                    <?php if( !get_sub_field('comming_soon') ):
                                        if( get_sub_field( 'address' ) ):?>
                                            <li class="address"><?php the_sub_field( 'address' ); ?></li>
                                        <?php endif;
                                        if( get_sub_field( 'phone_1' ) ):?>
                                            <li class="phone"><a href="tel:<?php echo str_replace( array(' ','(',')','-'), '', get_sub_field( 'phone_1' ) );?>"><?php the_sub_field( 'phone_1' ); ?></a></li>
                                        <?php endif;
                                        if( get_sub_field( 'phone_2' ) ):?>
                                            <li class="phone"><a href="tel:<?php echo str_replace( array(' ','(',')','-'), '', get_sub_field( 'phone_2' ) );?>"><?php the_sub_field( 'phone_2' ); ?></a></li>
                                        <?php endif;
                                                                                    if( get_sub_field( 'fax' ) ):?>
                                            <li class="tel-fax"><?php the_sub_field( 'fax' ); ?></li>
                                        <?php endif;
                                    else: ?>
                                    <li class="coming-soon"><?php _e('Coming Soon','isimulate'); ?></li>
                                    <?php endif;?>
                                </ul>
                            </div><!--/.contact-address-box-->
                        </div><!--/.col-->
                    <?php endwhile; ?>
		        </div><!--/.cols-->
                <?php endif;?>
            </div><!--/.contact-main-box-->    
		</div><!--/.wrap -->
    </div><!--/.download-main-box -->
     <div class="contact-distributors-list">
		<?php //distrinutor
		get_template_part( 'page-contents/content', 'distributors' );
		?>
    </div>
    <div class="contact-form-main-box" id="inTouch">
        <div class="wrap">
        	<?php if( get_field('contact_form_title') || get_field('contact_form_sub_title') ):?>
	            <h2 class="section-title">
                	<?php if( get_field('contact_form_title') ): ?>
                	<span><?php the_field('contact_form_title'); ?></span>
                    <?php endif;
					if( get_field( 'contact_form_sub_title' ) ):
                    	the_field( 'contact_form_sub_title' );
					endif;?>
                </h2>
            <?php endif;
			if( get_field( 'contact_form_short_description' ) ): ?>
            <div class="product-box">
            	<p><?php the_field( 'contact_form_short_description' ); ?></p>
            </div>
            <?php endif; ?>
            <div class="contact-form-box">
           	<?php the_content();?>
            <!--/.contact-form--> 
          	</div>
            <!--/.contact-form-box-->
        </div><!--/.wrap-->
    </div>
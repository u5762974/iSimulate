<?php
/**
 * The template for displaying home page
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/?>
    <div class="our-product-box reality-video">
        <div class="wrap">
            <?php if( get_field( 'upcoming_about_title' ) || get_field( 'upcoming_about_sub_title' ) ):?>
                <h2 class="section-title">
                    <?php if( get_field( 'upcoming_about_title' ) ): ?>
                    <span><?php the_field( 'upcoming_about_title' ); ?></span>
                    <?php endif;
                    if( get_field( 'upcoming_about_sub_title' ) ):
                        the_field( 'upcoming_about_sub_title');
                    endif; ?>
                </h2>
            <?php endif; 
            if( get_field( 'upcoming_about_description' ) ):?>
                <div class="product-box">
                    <?php the_field( 'upcoming_about_description' ); ?>
                </div>
            <?php endif;?>
            <?php if( $video_id = get_field( 'upcoming_youtube_video_id' ) ): ?>
                <div class="product-video-box"><iframe src="https://www.youtube.com/embed/<?php echo $video_id; ?>" width="640" height="360" frameborder="0" allowfullscreen></iframe></div>
            <?php endif; ?>
        </div><!--/.wrap -->
    </div><!--/.history-main-box -->
    <?php //Product Highlights section ?>
    <?php if( have_rows( 'upcoming_about_features' ) ): //To check product highlights ?>
    	<div class="product-feature-list-box realitiproduct-listing">
            <div class="wrap">
                <div class="cols cols3">
                	<?php while( have_rows( 'upcoming_about_features' ) ) : the_row(); //Loop Start ?>
                        <div class="col">
                            <div class="product-feature-details-box">
                                <h2><?php echo $num_padded = sprintf( "%02d", ++$counter );?></h2>
                                <div class="product-feature-details">
                                	<?php if( get_sub_field( 'title' ) ):?>
	                                    <h5><?php the_sub_field('title'); ?></h5>
                                    <?php endif;?>
                                </div>
                            </div><!--/.product-feature-box-details -->
                        </div><!--/.col -->
                    <?php endwhile; ?>
                </div>
            </div><!--/.wrap-->
        </div><!--/.product-feature-list-box -->
    <?php endif;
    //request a quote section
    get_template_part( 'page-contents/content', 'request-quote' );?>
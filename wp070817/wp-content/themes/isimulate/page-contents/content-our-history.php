<?php
/**
 * The template for displaying home page
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/
 
 	//Out history section
	if( get_field('our_history_title') || get_field('our_history_sub_title') ): ?>
        <div class="history-main-box">
            <div class="wrap">
            	<?php if( get_field('our_history_title') || get_field('our_history_sub_title') ): ?>
                        <h2 class="section-title white">
                        <?php if(get_field('our_history_title')):?>
                            <span><?php the_field('our_history_title');?></span>
                        <?php endif;
    
                        if( get_field('our_history_sub_title') ): 
                            the_field('our_history_sub_title');
                        endif; ?>
                        </h2>
                <?php endif;
				if( get_field('our_history_description') || get_field('our_history_button_link') ):?>
                    <div class="history-box">
                    	<?php if( get_field('our_history_description') ):?>
	                        <p><?php the_field('our_history_description'); ?></p>
                        <?php endif; ?>
                        <a href="<?php the_field('our_history_button_link');?>" class="button btn-outline readmore"><?php _e('Read More','isimulate'); ?></a>
                    </div>
                <?php endif; ?>
            </div><!--/.wrap -->
        </div><!--/.history-main-box -->
    <?php endif;?>
<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
	<div class="download-main-box">
        <div class="wrap">
            <?php if( get_field( 'page_title' ) || get_field( 'page_sub_title' ) ):?>
                <h2 class="section-title">
                    <?php if( get_field( 'page_title' ) ): ?>
                        <span><?php the_field( 'page_title' ); ?></span>
                    <?php endif;
                    if( get_field( 'page_sub_title' ) ):
                        the_field( 'page_sub_title' );
                    endif;?>
                </h2>
            <?php endif; ?>
            <div class="product-box">
                <?php the_content(); ?>
            </div>
        </div><!--/.wrap -->
        <?php if( have_rows( 'download_details' ) ): //To check product highlights ?>
            <div class="download-box">
                <div class="wrap">
                    <div class="cols cols4">
                    	<?php while( have_rows( 'download_details' ) ) : the_row(); //Loop Start
							if( get_sub_field( 'pdf_file' ) && get_sub_field( 'title' ) ): ?>
                                <div class="col">
                                    <div class="pdf-download-box">
                                        <a href="<?php echo get_sub_field( 'pdf_file' );?>" target="new">
                                            <div class="pdf-download">
                                                <span class="pdf-icon"><?php _e('PDF','isimulate'); ?></span>
                                                <h5><?php echo get_sub_field( 'title' ); ?></h5>
                                            </div><!--/.pdf-download-->
                                        </a>
                                    </div>
                                </div><!--/.col-->
                        <?php endif;
						endwhile; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
  	</div><!--/.download-main-box -->
    <?php //request a quote section
    get_template_part( 'page-contents/content', 'request-quote' );?>
	
<?php
/**
 * The template for displaying our product section
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/
 
 	//Our product section ?>
    
		    <div class="product-list-box">
                <div class="wrap">
                	<?php if( get_field( 'our_product_title' ) || get_field( 'our_product_sub_title' ) ): ?>
	                	<h2 class="section-title">
                        	<?php if( get_field('our_product_title')): ?>
	                        	<span><?php the_field('our_product_title'); ?></span>
							<?php endif;
							if( get_field('our_product_sub_title') ): 
								the_field('our_product_sub_title');
							endif;?>
                        </h2>
                    <?php endif; ?>
                    
                    <?php if( have_rows('home_products') ): ?>
                        <div class="product-list">
                            <div class="cols cols5">
                            	<?php  while ( have_rows('home_products') ) : the_row(); ?>
	                                <div class="col">
                                    <div class="product-list-details-box">
                                    	<?php $product_link = get_sub_field('product_link')? get_sub_field('product_link') : '#';?>
                                        <a href="<?php echo $product_link; ?>">
                                            <div class="product-list-image-box">
                                            	<?php if( $product_logo_image = get_sub_field( 'product_logo' ) ):  //to add product logo ?>
                                                    <div class="product-logo">
                                                        <img src="<?php echo $product_logo_image['url']; ?>" alt="<?php echo get_the_title(); ?>">
                                                    </div><!--/.product-logo -->
                                                <?php endif;
                                                $nologo = (!$product_logo_image['url']) ? ' no-logo':''; ?>
                                                
                                                <div class="product-list-image<?php echo $nologo;?>">
                                                <?php if( $product_image = get_sub_field( 'image' ) ):  //to add product thumb ?>
													<img src="<?php echo $product_image['sizes']['product-thumb']; ?>" alt="<?php echo get_sub_field('title'); ?>">
												<?php else:?>
                                                 	<img src="<?php echo ISIMULATE_DEFAULT_PRODUCT_THUMB; ?>" alt="<?php echo get_sub_field('title'); ?>">
                                                <?php endif; ?>
                                                </div>
                                            </div><!--/.product-list-image-box -->
                                            <?php if( get_sub_field('title') ):?>
                                           		<h3><?php echo get_sub_field('title'); ?></h3>
                                           	<?php endif; ?>
                                        </a>
                                    </div><!--/.product-list-details-box -->
                                </div><!--/.col -->
                                <?php endwhile; ?>
                            </div><!--/.cols -->
                        </div><!--/.product-list -->
                    <?php endif; ?>
                </div><!--/.wrap -->
            </div><!--/.product-list-box -->
    
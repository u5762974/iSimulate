<?php
/**
 * The template for displaying our product section
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/?>
 	<div class="our-product-box">
            <div class="wrap">
                <?php if( get_field( 'page_title' ) || get_field( 'page_sub_title' ) ):?>
                <h2 class="section-title">
                    <?php if( get_field( 'page_title' ) ): ?>
                        <span><?php the_field( 'page_title' ); ?></span>
                    <?php endif;
                    if( get_field( 'page_sub_title' ) ):
                        the_field( 'page_sub_title' );
                    endif;?>
                </h2>
	        <?php endif; 
		if( get_the_content()):?>
                    <div class="product-box">
                        <?php the_content(); //To retrive content ?>
                    </div>
                <?php endif; ?>
			
                <?php // Product addons section 
                if( have_rows( 'product_addons' ) ): //To check product addons ?>
                <div class="product-details-list">
                    <div class="cols cols4">
                        <?php while( have_rows( 'product_addons' ) ) : the_row(); //Loop Start ?>
                            <div class="col">
                                <div class="product-details-box">
                                    <div class="product-image-box">
                                        <?php if($addon_image = get_sub_field( 'image' ) ): //To check addon image ?>
                                            <img alt="<?php echo get_sub_field( 'title' ); ?>" src="<?php echo $addon_image['url']; ?>">
                                        <?php else:?>
                                            <img alt="<?php echo get_sub_field( 'title' ); ?>" src="<?php echo ISIMULATE_ADDON_DEFAULT_IMAGE; ?>">
                                        <?php endif; ?>
                                    </div><!--/.product-image-box -->
                                    <?php if( get_sub_field( 'title' ) ): // To retrive addon title ?>
                                        <h3><?php the_sub_field( 'title' ); ?></h3>
                                    <?php endif; ?>
                                </div><!--/.product-details-box -->
                            </div><!--/.col -->
                        <?php endwhile; // end loop ?>
                    </div>
                </div><!--/.product-details-list -->
  			<?php endif; ?>
        </div><!--/.wrap -->
    </div><!--/.history-main-box -->
    
    <?php //Product Highlights section ?>
    <?php if( have_rows( 'product_highlights' ) ): //To check product highlights ?>
    	<div class="product-feature-list-box" style="background-image:url(<?php echo ISIMULATE_THEME_URL?>/images/bgi/feature-box-bg.jpg)">
            <div class="wrap">
                <div class="cols cols2">
                	<?php while( have_rows( 'product_highlights' ) ) : the_row(); //Loop Start ?>
                        <div class="col">
                            <div class="product-feature-details-box">
                                <h2><?php echo $num_padded = sprintf( "%02d", ++$counter );?></h2>
                                <div class="product-feature-details">
                                	<?php if( get_sub_field( 'title' ) ):?>
	                                    <h5><?php the_sub_field('title'); ?></h5>
                                    <?php endif;
									if( get_sub_field( 'description' ) ):
										the_sub_field( 'description' );
									endif; ?>
                                </div>
                            </div><!--/.product-feature-box-details -->
                        </div><!--/.col -->
                    <?php endwhile; ?>
                </div>
            </div><!--/.wrap-->
        </div><!--/.product-feature-list-box -->
    <?php endif; ?>

    <?php //Request a Quote Section
	if( get_field( 'product_raq_button_link','option' ) ): ?>
        <div class="ready-request-quote">
            <div class="wrap">
            	<?php if( get_field( 'product_raq_title','option' ) || get_field( 'product_raq_sub_title','option' ) ):?>
	                <h2>
                    	<?php if( get_field( 'product_raq_title','option' ) ): ?>
                    		<span><?php the_field( 'product_raq_title', 'option' ); ?></span>
                       	<?php endif;
						if( get_field( 'product_raq_sub_title','option' )):
							the_field( 'product_raq_sub_title','option' );
						endif;?>
                  	</h2>
                <?php endif;
				$button_raq_text = get_field( 'product_raq_button_text','option' ) ? get_field( 'product_raq_button_text','option' ) : __( 'Request a Quote','isimulate' ); ?>
                <a class="button btn-lg request-btn" href="<?php echo get_field('product_raq_button_link','option' ); ?>"><?php echo $button_raq_text; ?></a>
            </div><!--/.wrap-->
        </div><!--/.ready-request-quote-->
	<?php endif; ?>

	<?php //product detail description 
	if( get_field('product_page_feature_title') || get_field('product_page_feature_sub_title') || get_field('product_page_feature_description') ):?>
	<div class="product-feature-about-box" style="background-image:url(<?php echo ISIMULATE_THEME_URL;?>/images/bgi/product-feature.jpg);">
        <div class="wrap">
            <h2 class="section-title">
		            <?php if( get_field( 'product_page_feature_title' ) ): //To check product page title?>
        	        	<span><?php the_field( 'product_page_feature_title' );?></span>
                    <?php endif;
					if( get_field( 'product_page_feature_sub_title' ) ): //To check product page sub title
						the_field( 'product_page_feature_sub_title' );
					endif;?>
            </h2>
            <?php if( get_field('product_page_feature_description') ): ?>
                <div class="product-feature-about">
                    <?php the_field('product_page_feature_description'); ?>
                </div><!--/.product-feature-about-->
            <?php endif; ?>
        </div><!--/.wrap-->
	</div><!--/.product-feature-about-box-->
    <?php endif; ?>

    <?php //Product waveforms ?>
    <?php if( get_field('product_feature_list') || get_field( 'waveforms_page_link' )  ): //To check product highlights ?>
        <div class="waveforms-list-box">
            <div class="wrap">
                <div class="waveforms-box list-block">
                	<?php //To retrive feature list
					if( get_field('product_feature_list') ):

	                	the_field('product_feature_list');
					endif;
					if( get_field( 'waveforms_page_link' ) ):?>
                        <div class="aligncenter">
                            <a class="button btn-lg request-btn" href="<?php echo get_field( 'waveforms_page_link' );?>"><?php _e( 'View All Waveforms','isimulate' );?></a>
                        </div><!--/.aligncenter-->
                    <?php endif; ?>
                </div><!--/.waveforms-box-->
            </div><!--/.wrap-->
        </div><!--/.waveforms-list-box-->
    <?php endif; ?>
    
  	<?php //activewave section
	if( get_field( 'activewave_image' ) || get_field( 'activewave_description' ) ):?>
        <div class="waves-details-box">
            <div class="wrap">
                <div class="cols cols2">
                    <div class="col">
                    	<?php if( $activewaves_image = get_field( 'activewave_image' ) ):?>
                            <div class="waves-images-box">
                                <img src="<?php echo $activewaves_image['url']; ?>" alt="<?php echo get_the_title();?>">
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col">
                    	<?php if( get_field( 'activewave_description' ) ):?>
                            <div class="wavew-details">
                                <?php the_field( 'activewave_description' ); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

	<?php //home page testimonials
	get_template_part( 'page-contents/content', 'testimonials' );
	
	//request a quote section
	get_template_part( 'page-contents/content', 'request-quote' );
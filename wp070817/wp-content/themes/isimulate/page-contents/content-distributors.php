<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
    <div class="wrap">
        <?php if( get_field( 'page_title',ISIMULATE_DISTRIBUTOR_PAGE ) || get_field( 'page_sub_title',ISIMULATE_DISTRIBUTOR_PAGE ) ):?>
            <h2 class="section-title">
                <?php if( get_field( 'page_title',ISIMULATE_DISTRIBUTOR_PAGE ) ): ?>
                    <span><?php the_field( 'page_title',ISIMULATE_DISTRIBUTOR_PAGE ); ?></span>
                <?php endif;
                if( get_field( 'page_sub_title',ISIMULATE_DISTRIBUTOR_PAGE ) ):
                    the_field( 'page_sub_title',ISIMULATE_DISTRIBUTOR_PAGE );
                endif;?>
            </h2>
        <?php endif; ?>
    </div><!--/.wrap -->
	<?php if( have_rows( 'distributors',ISIMULATE_DISTRIBUTOR_PAGE ) ): //To check main features ?>
        <div class="distributors-list-box">
            <div class="wrap">
                <div class="distributors-list">
                	<div class="cols cols3">
                    <?php while( have_rows( 'distributors',ISIMULATE_DISTRIBUTOR_PAGE ) ) : the_row(); //Loop Start ?>
                        <div class="col">
                        	<div class="distributors-address-info-box">
                               	<div class="distributors-address-box">
                                	<?php if( get_sub_field( 'country' ) ): ?>
        	                        	<h5><?php the_sub_field( 'country' ); ?></h5>
                                    <?php endif;
									if( get_sub_field( 'company_name' )):?>
    	                                <h6><?php the_sub_field( 'company_name' ); ?></h6>
                                    <?php endif;
									if( get_sub_field( 'address' ) ): ?>
	                                    <address><?php the_sub_field( 'address' ); ?></address>
                                    <?php endif;
									if( get_sub_field( 'phone_no_1' ) || get_sub_field( 'phone_no_2' ) || get_sub_field( 'fax_no' )  ): ?>
                                        <ul class="desctibuter-contatc">
                                        	<?php if( get_sub_field( 'phone_no_1' ) ):?>
	                                            <li class="phone"><a href="tel:<?php echo str_replace( array(' ','(',')'), '', get_sub_field( 'phone_no_1' ));?>"><?php the_sub_field( 'phone_no_1' );?></a></li>
                                            <?php endif;
											if( get_sub_field( 'phone_no_2' ) ):?>
	                                            <li class="phone"><a href="tel:<?php echo str_replace( array(' ','(',')'), '', get_sub_field( 'phone_no_2' ));?>"><?php the_sub_field( 'phone_no_2' );?></a></li>
                                            <?php endif;
                                            if( get_sub_field( 'fax_no' ) ):?>
	                                            <li class="fax"><?php the_sub_field( 'fax_no' ); ?></li>
                                            <?php endif;?>
                                        </ul>
                                    <?php endif; ?>
                                </div><!-- /.distributors-address-box -->
                           	</div><!--/.distributors-address-info-box-->
                        </div><!--/.col -->
                    <?php endwhile; ?>
                    </div><!--/.cols -->
                </div><!--/.distributors-list -->
            </div><!--/.wrap-->
        </div><!--/.distributors-list-box -->
    <?php endif; ?>

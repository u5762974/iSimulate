<?php
/**
 * The template part for displaying a message that your story posts cannot be found
 *
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Nothing Found', 'isimulate' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'isimulate' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

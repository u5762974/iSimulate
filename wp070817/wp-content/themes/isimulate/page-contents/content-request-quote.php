<?php
/**
 * The template for displaying request a quote
 *
 * This is the template that displays request a quote section
 *
 * @since iSimulate 1.0
 **/
 	if( get_field( 'req_quote_title','option' ) || get_field( 'req_quote_sub_title','option' ) || get_field('req_quote_description','option' ) || get_field('req_quote_button_link','option' ) ):
	 	$background = get_field( 'req_quote_background_image','option' ) ? ' style= "background-image: url('.get_field( 'req_quote_background_image','option' ).')"' :'';?>
        <div class="request-main-box"<?php echo $background; ?>>
            <div class="wrap">
                <?php if( get_field( 'req_quote_title','option' ) ):?>
                    <h2 class="section-title">
                        <?php if( get_field( 'req_quote_title','option' ) ): //To retrive title ?>
                            <span><?php the_field( 'req_quote_title','option' ); ?></span>
                        <?php endif;
                        if( get_field( 'req_quote_sub_title','option' ) ): //To retrive sub title 
                            the_field( 'req_quote_sub_title','option' );
                        endif; ?>
                    </h2>
                <?php endif;
                if( get_field( 'req_quote_description','option' ) || get_field( 'req_quote_button_link','option' ) ): ?>
                <div class="request-quote-box">
                    <?php if( get_field( 'req_quote_description','option' ) ): ?>
                        <p><?php the_field( 'req_quote_description','option' ); ?></p>
                    <?php endif;
                    $button_link = ( get_field('req_quote_button_text','option' ) )? get_field( 'req_quote_button_text','option' ) : 'Request a Quote';
                    if($button_link && get_field('req_quote_button_link','option' ) ):?>
                        <a href="<?php echo get_field('req_quote_button_link','option' );?>" class="button btn-lg request-btn"><?php echo $button_link;?></a>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
            </div><!--/.wrap -->
        </div><!--/.request-main-box -->
    <?php endif;?>
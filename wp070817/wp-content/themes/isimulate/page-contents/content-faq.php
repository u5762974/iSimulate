<?php
/**
 * The template for displaying contact page
 *
 * This is the template that displays contact page content
 *
 * @since iSimulate 1.0
 **/
?>
	<div class="download-main-box">
        <div class="wrap">
        	<?php if( get_field( 'page_title' ) || get_field( 'page_sub_title' ) ):?>
	            <h2 class="section-title">
                	<?php if( get_field( 'page_title' ) ): ?>
	                	<span><?php the_field( 'page_title' ); ?></span>
                    <?php endif;
					if( get_field( 'page_sub_title' ) ):
						the_field( 'page_sub_title' );
					endif;?>
               	</h2>
            <?php endif; ?>
            <div class="product-box">
                <?php the_content(); ?>
            </div>
        </div><!--/.wrap -->
		<?php if( have_rows( 'faq' ) ): //To check main features ?>
            <div class="download-box">
                <div class="wrap">
                    <div class="accordion-databox faq">
						<?php while( have_rows( 'faq' ) ) : the_row(); //Loop Start ?>
                            <div class="accordion-row">
                            	<?php if(get_sub_field('question') && get_sub_field('answer') ):?>
                                <h5 class="accordion-trigger"><?php the_sub_field('question'); ?></h5>
                                <div class="accordion-data">
                                	<?php if( $image = get_sub_field('image') ):?>
	                                    <figure><img alt="<?php echo $image['alt']; ?>" src="<?php echo $image['url']; ?>"></figure>
                                    <?php endif; 
									the_sub_field('answer'); ?>
									
                                </div><!--/.accordion-data -->
                                <?php endif; ?>
                            </div><!--/.accordion-row -->
						<?php endwhile; ?>
                    </div><!--/.accordion-databox -->
                        
                </div><!--/.wrap-->
            </div><!--/.download-main-box-->
        <?php endif; ?>
    </div><!--/.download-main-box -->
<?php
/**
 * The template for displaying your story single entry box
 *
 * This is the template that displays your story single entry box content
 *
 * @since iSimulate 1.0
 **/
		//$even_odd = (strcmp($even_odd,'even')) ? 'odd' : 'even'; //Even Odd Class?>
		<div class="story-box">
                        
			<div class="story-image">
				<a href="<?php echo get_permalink();?>">
					<?php if( has_post_thumbnail(get_the_ID()) ) : //Check Has Post Thumbnail?>
						<img src="<?php the_post_thumbnail_url( 'your-story-thumb' )?>"/>
					<?php else : //Else Default Image ?>
						<img src="<?php echo ISIMULATE_DEFAULT_VIDEO_THUMB; ?>" alt="<?php the_title();?>">
					<?php endif; //Endif ?>
				</a>
			</div><!--/.story-image-->
			<?php //to load post technology
				$tech_list='';
				$arrTech=array();
				$post_technology = get_the_terms( get_the_ID(), 'group' );
				if($post_technology):
					foreach($post_technology as $technology):
						$arrTech[]= $technology->name;
					endforeach;
					$technology_list = implode(',',$arrTech);
				endif;
				
				//To load post country
				$country_list='';
				$arrCountry=array();
				$post_country = get_the_terms( get_the_ID(), 'country' );
				if($post_country):
					foreach($post_country as $country):
						$arrCountry[]= $country->name;
					endforeach;
					$country_list = implode(',',$arrCountry);
					if(strlen( $technology_list) > 0):
						$country_list=', '.$country_list;
					endif;
				endif;
				?>
                <div class="stroy-details-box">
                    <h5><a href="<?php echo get_permalink();?>"><?php the_title();?></a><span><?php echo $technology_list.$country_list; ?></span></h5>
                     <p><?php echo wp_trim_words( get_the_content(),40 ); //Get content for news post ?></p>
                    <a class="button btn-secondary read-story" href="<?php echo get_permalink();?>">Read More</a>
                </div><!--/.stroy-details-box-->
		</div><!--/.story-box-->
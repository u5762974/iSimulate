<?php
/**
 * The template for displaying our heartisense product content
 *
 * This is the template for displaying our heartisense product content
 *
 * @since iSimulate 1.0
 **/ 
?>
    <div class="our-product-box">

        <div class="wrap">
            <?php if( get_field('exp_cpr_training_title') || get_field('exp_cpr_training_subtitle') || get_field('exp_cpr_training_content') ) : //Check Title or Subtitle or Content
                    if( get_field('exp_cpr_training_title') || get_field('exp_cpr_training_subtitle') ) : //Check Training Title or Subtitle ?>
                        <h2 class="section-title"><?php printf('%1$s %2$s', '<span>'.get_field('exp_cpr_training_title').'</span>', get_field('exp_cpr_training_subtitle') );?></h2>
                    <?php endif; //Endif
                     //Check Content Set and Learn More Button Set
                    if( get_field('exp_cpr_training_content') || ( get_field('exp_cpr_training_learn_more_btn_link') && get_field('exp_cpr_training_learn_more_btn_text') ) ) : ?>
                        <div class="product-box">
                            <?php the_field('exp_cpr_training_content'); //Content of Training                        
                            if( get_field('exp_cpr_training_learn_more_btn_link') && get_field('exp_cpr_training_learn_more_btn_text') ) : //Check Button Link and Button Text Set ?>
                                <a class="button btn-lg request-btn" href="<?php echo esc_url( get_field('exp_cpr_training_learn_more_btn_link') );?>"><?php the_field('exp_cpr_training_learn_more_btn_text');?></a>
                            <?php endif; //Endif ?>
                        </div><!--/.product-box-->
                    <?php endif; //Endif
                endif; ///Endif ?>

            <hr class="tall-space dotted-border">

            <?php if( get_the_content() ) : ?>
                <div class="save-life-description">
                    <?php the_content(); //Content of The Page ?>
                </div><!--/.save-life-description-->
            <?php endif; //Endif ?>
        </div><!--/.wrap -->
        
        <?php if( get_field('structure_title') || get_field('structure_subtitle') || get_field('structure_content') || have_rows('heartisense_structures') ) : //Check Title or Subtitle ?>
            <div class="heartisense-structure-box">
                <div class="wrap">
                    <?php if( get_field('structure_title') || get_field('structure_subtitle') ) : //Check Title or Subtitle ?>
                        <h2 class="section-title"><?php printf('%1$s %2$s', '<span>'.get_field('structure_title').'</span>', get_field('structure_subtitle') );?></h2>
                    <?php endif; //Endif 
                        if( get_field('structure_content') ) : //Check Structure Content
                            the_field('structure_content'); //Structure Content
                        endif; //Endif
                    if( have_rows('heartisense_structures') ) : //Check Structures Set ?>
                        <div class="heartisense-structure-details">
                            <div class="cols cols2">
                                <?php while( have_rows('heartisense_structures') ) : the_row(); //Loop to List Structure
                                    if( get_sub_field('image') || get_sub_field('video_link') ) : //Check Image or Video Link ?>
                                        <div class="col">
                                            <div class="heartisense-structure">
                                                <?php if( get_sub_field('img_video') == 'img' ) : //Check Image or Video
                                                    $img = get_sub_field('image'); //Image for Structure ?>
                                                    <img src="<?php echo $img['url'];?>" alt="<?php echo $img['title'];?>" />
                                                <?php else : //Else video
                                                    $video = get_sub_field('video_link'); //Video Link ?>
                                                    <iframe width="560" height="315" src="<?php echo esc_url($video);?>" frameborder="0" allowfullscreen></iframe>
                                                <?php endif; //Endif ?>
                                            </div><!--/.heartisense-structure-->
                                        </div><!--/.col-->
                                <?php endif; //Endif
                                endwhile; //Endwhile ?>
                            </div><!--/.cols2-->
                        </div><!--/.heartisense-structure-details-->
                    <?php endif; //Endif ?>
                </div><!--/.wrap-->
           </div><!--/.heartisense-structure-box-->
       <?php endif; //Endif
       
        if( get_field('feature_title') || get_field('feature_subtitle') || have_rows('heartisense_features') ) :  //Check Feature Details Set ?>
            <div class="heartisense-features-main-box">
                <div class="wrap">
                    <?php if( get_field('feature_title') || get_field('feature_subtitle') ) : //Check Title or Subtitle ?>
                        <h2 class="section-title"><?php printf('%1$s %2$s', '<span>'.get_field('feature_title').'</span>', get_field('feature_subtitle') );?></h2>
                    <?php endif; //Endif
                    if( $img = get_field('feature_image') ) : //Check Image or Caption Set ?>
                        <div class="image-block">
                            <figure><img src="<?php echo $img['url'];?>" alt="<?php echo $img['title'];?>" />
                                <?php if( $caption = get_field('feature_img_caption') ) : //Check Image Caption ?>
                                    <figcaption><?php echo $caption; ?></figcaption>
                                <?php endif; //Endif ?>
                            </figure>
                        </div><!--/.image-block-->
                    <?php endif; //Endif ?>

                    <hr class="tall-space dotted-border">

                    <?php if( have_rows('heartisense_features') ) : //Check Features ?>
                        <div class="heartisense-features-box">
                             <div class="cols cols3">
                                <?php while( have_rows('heartisense_features') ) : the_row(); //Loop to List Features ?>
                                    <div class="col">
                                         <div class="heartisense-features<?php echo get_sub_field('icon') ? ' '.get_sub_field('icon') : ''; //Icon ?>">
                                            <?php if( $title = get_sub_field('title') ) : //Check Title ?>
                                                <h4><?php echo $title;?></h4>
                                            <?php endif; //Endif
                                            if( $img = get_sub_field('image') ) : //Check Image Set ?>
                                                <img src="<?php echo $img['url'];?>" alt="<?php echo $img['title'];?>">
                                            <?php endif; //Endif
                                            if( $content = get_sub_field('content') ) : //Check Content ?>
                                                <p><?php echo $content;?></p>
                                            <?php endif; //Endif ?>
                                         </div><!--/.heartisense-features-->
                                    </div><!--/.col-->
                                <?php endwhile; //Endwhile ?>
                             </div><!--/.cols3-->
                        </div><!--/.heartisense-features-box-->
                    <?php endif; //Endif ?>
                </div><!--/.wrap-->
            </div><!--/.heartisense-features-main-box-->
        <?php endif; //Endif ?>
    </div><!--/.our-product-box-->
    
    <?php if( get_field('kit_title') || get_field('kit_subtitle') || get_field('kit_intro_content') || have_rows('kit_modules') ) : //Check Heartisense Structure Box ?>
        <div class="heartisense-structure-box">
            <div class="wrap">
                <?php if( get_field('kit_title') || get_field('kit_subtitle') ) : //Check Title or Subtitle ?>
                    <h2 class="section-title"><?php printf('%1$s %2$s', '<span>'.get_field('kit_title').'</span>', get_field('kit_subtitle') );?></h2>
                <?php endif; //Endif
                if( get_field('kit_intro_content') ) : //Check Intro Content Set
                    the_field('kit_intro_content'); //Intro Content
                endif; //Endif

                if( have_rows('kit_modules') ) : //Check Modules Set ?>
                    <div class="heartisense-kit-box">
                        <div class="cols cols3">
                            <?php while( have_rows('kit_modules') ) : the_row(); //Loop to List Modules ?>
                                <div class="col">
                                    <div class="heartisense-features">
                                        <?php if( $image = get_sub_field('image') ) : //Check Image Set ?>
                                            <img src="<?php echo $image['url'];?>" alt="<?php the_sub_field('name');?>">
                                        <?php endif; //Endif
                                        if( get_sub_field('name') ) : //Check Name Set ?>
                                            <h4><?php the_sub_field('name');?></h4>
                                        <?php endif; //Endif
                                        if( get_sub_field('description') ) : //Check Name Set ?>
                                            <p><?php the_sub_field('description');?></p>
                                        <?php endif; //Endif ?>
                                    </div><!--/.heartisense-features-->
                                </div><!--/.col-->
                            <?php endwhile; //Endwhile ?>
                        </div><!--/.cols3-->
                    </div><!--/.heartisense-kit-->
                <?php endif; //Endif ?>
            </div><!--/.wrap-->
        </div><!--/.heartisense-structure-box-->    
    <?php endif; //Endif
    
    if( get_field('app_title') || have_rows('apps_list') ) : //Check App Title Set or Apps Rows Set ?>
        <div class="heartisense-app-main-box">
            <div class="wrap">
                <div class="heartisense-app-box">
                    <?php if( get_field('app_title') ) : //Check App Title Set ?>
                        <h2 class="section-title"><?php the_field('app_title');?></h2>
                    <?php endif; //Endif

                    if( have_rows('apps_list') ) : //Check Apps List Set ?>
                        <div class="heartisense-app">
                            <?php $counter = 1;
                            $odd_even = '';
                            while( have_rows('apps_list') ) : the_row(); //Loop to List Apps ?>
                                <div class="heartisense-box">
                                    <?php $images = get_sub_field('images');                                        
                                        if( count( $images ) <= 1 ) : //Check Image Set
                                            while( have_rows('images') ) : the_row(); //Loop to List Images
                                                if( $image = get_sub_field('image') ) : //Check Image ?>
                                                    <div class="heartisense-image <?php echo $odd_even;?>">
                                                        <img src="<?php echo $image['url'];?>" alt="<?php echo $img['title'];?>">
                                                    </div><!--/.heartisense-image-->
                                        <?php endif; //Endif
                                            endwhile; //Endwhile                                                    
                                        else : //Else  ?>
                                            <div class="heartisense-image <?php echo $odd_even;?>">
                                                <div class="heartisense-slider">
                                                    <?php while( have_rows('images') ) : the_row(); //Loop to List Images
                                                        if( $image = get_sub_field('image') ) : //Check Image ?>
                                                            <div class="slide-item">
                                                                <a href="<?php echo $image['url'];?>" class="venoboxvid vbox-item" data-gall="gall<?php echo $counter;?>"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"></a>
                                                            </div><!--/.slide-item-->
                                                    <?php endif; //Endif
                                                    endwhile; //Endwhile ?>
                                                </div><!--/.heartisense-slider-->
                                            </div><!--/.heartisense-image-->
                                    <?php endif; //Endif
                                    if( get_sub_field('content') ) : //Check Description Set ?>
                                        <div class="heartisense-details-box <?php echo $odd_even;?>">
                                            <?php the_sub_field('content'); //Description of App ?>
                                        </div><!--/.heartisense-details-box-->
                                    <?php endif; //Endif ?>
                                </div><!--/.heartisense-box-->
                            <?php $counter++; //Counter
                                $odd_even = $odd_even == 'odd' ? '' : 'odd'; //Odd Even
                            endwhile; //Endwhile ?>
                        </div><!--/.heartisense-app-->
                    <?php endif; //Endif ?>
                </div><!--/.heartisense-app-box-->
            </div><!--/.wrap-->
        </div><!--/.heartisense-app-main-box-->
    <?php endif; //Endif ?>
        
    <?php if( get_field('monitor_title') || have_rows('monitor_list') ) : //Check App Title Set or Apps Rows Set ?>
        <div class="heartisense-structure-box">
            <div class="wrap">
                <?php if( get_field('monitor_title') ) : //Check Monitor Title Set ?>
                    <h2 class="section-title"><?php the_field('monitor_title');?></h2>
                <?php endif; //Endif
                if( have_rows('monitor_list') ) : //Check Monitor List ?>
                    <div class="heartisense-monitor">
                        <?php $counter = 1;
                            $odd_even = '';
                            while( have_rows('monitor_list') ) : the_row(); //Loop to List Monitor ?>
                                <div class="heartisense-box">
                                    <?php $images = get_sub_field('images');                                        
                                        if( count( $images ) <= 1 ) : //Check Image Set
                                            while( have_rows('images') ) : the_row(); //Loop to List Images
                                                if( $image = get_sub_field('image') ) : //Check Image ?>
                                                    <div class="heartisense-image <?php echo $odd_even;?>">
                                                        <img src="<?php echo $image['url'];?>" alt="<?php echo $img['title'];?>">
                                                    </div><!--/.heartisense-image-->
                                        <?php endif; //Endif
                                            endwhile; //Endwhile
                                        else : //Else  ?>
                                            <div class="heartisense-image <?php echo $odd_even;?>">
                                                <div class="heartisense-slider">
                                                    <?php while( have_rows('images') ) : the_row(); //Loop to List Images
                                                        if( $image = get_sub_field('image') ) : //Check Image ?>
                                                            <div class="slide-item">
                                                                <a href="<?php echo $image['url'];?>" class="venoboxvid vbox-item" data-gall="gall<?php echo $counter;?>"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"></a>
                                                            </div><!--/.slide-item-->
                                                    <?php endif; //Endif
                                                    endwhile; //Endwhile ?>
                                                </div><!--/.heartisense-slider-->
                                            </div><!--/.heartisense-image-->
                                    <?php endif; //Endif
                                    if( get_sub_field('content') ) : //Check Description Set ?>
                                        <div class="heartisense-details-box <?php echo $odd_even;?>">
                                            <?php the_sub_field('content'); //Description of App ?>
                                        </div><!--/.heartisense-details-box-->
                                    <?php endif; //Endif ?>
                                </div><!--/.heartisense-box-->
                            <?php $counter++; //Counter
                                $odd_even = $odd_even == 'odd' ? '' : 'odd'; //Odd Even
                            endwhile; //Endwhile ?>
                    </div><!--/.heartisense-monitor-->
                <?php endif; //Endif ?>
            </div><!--/.wrap-->
        </div><!--/.heartisense-structure-box-->
    <?php endif; //Endif ?>
    
    <?php if( get_field('test_title') || have_rows('tests_list') ) : //Check App Title Set or Apps Rows Set ?>
        <div class="heartisense-app-main-box">
            <div class="wrap">
                <div class="heartisense-app-box">
                    <?php if( get_field('test_title') ) : //Check Test Title Set ?>
                        <h2 class="section-title"><?php the_field('test_title');?></h2>
                    <?php endif; //Endif
                    if( have_rows('tests_list') ) : //Check Test List ?>
                        <div class="heartisense-app">
                            <?php $counter = 1;
                            $odd_even = 'odd';
                            while( have_rows('tests_list') ) : the_row(); //Loop to List Tests ?>
                                <div class="heartisense-box">
                                    <?php $images = get_sub_field('images');
                                        if( count( $images ) <= 1 ) : //Check Image Set
                                            while( have_rows('images') ) : the_row(); //Loop to List Images
                                                if( $image = get_sub_field('image') ) : //Check Image ?>
                                                    <div class="heartisense-image <?php echo $odd_even;?>">
                                                        <img src="<?php echo $image['url'];?>" alt="<?php echo $img['title'];?>">
                                                    </div><!--/.heartisense-image-->
                                        <?php   endif; //Endif
                                            endwhile; //Endwhile
                                        else : //Else  ?>
                                            <div class="heartisense-image <?php echo $odd_even;?>">
                                                <div class="heartisense-slider">
                                                    <?php while( have_rows('images') ) : the_row(); //Loop to List Images
                                                        if( $image = get_sub_field('image') ) : //Check Image ?>
                                                            <div class="slide-item">
                                                                <a href="<?php echo $image['url'];?>" class="venoboxvid vbox-item" data-gall="gall<?php echo $counter;?>"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"></a>
                                                            </div><!--/.slide-item-->
                                                    <?php endif; //Endif
                                                    endwhile; //Endwhile ?>
                                                </div><!--/.heartisense-slider-->
                                            </div><!--/.heartisense-image-->
                                    <?php endif; //Endif
                                    if( get_sub_field('content') ) : //Check Description Set ?>
                                        <div class="heartisense-details-box <?php echo $odd_even;?>">
                                            <?php the_sub_field('content'); //Description of App ?>
                                        </div><!--/.heartisense-details-box-->
                                    <?php endif; //Endif ?>
                                </div><!--/.heartisense-box-->
                            <?php $counter++; //Counter
                                $odd_even = $odd_even == 'odd' ? '' : 'odd'; //Odd Even
                            endwhile; //Endwhile ?>
                        </div><!--/.heartisense-app-->
                    <?php endif; //Endif ?>
                </div><!--/.heartisense-app-box-->
            </div><!--/.wrap-->
        </div><!--/.heartisense-app-main-box-->
    <?php endif; //Endif

    if( get_field('lms_title') || have_rows('tests_list') ) : //Check App Title Set or Apps Rows Set ?>

        <div class="heartisense-structure-box">
            <div class="wrap">
                <?php if( get_field('lms_title') ) : //Check Title ?>
                    <h2 class="section-title"><?php the_field('lms_title');?></h2>
                <?php endif; //Endif
                
                if( have_rows('lms_left_col_images') || get_field('lms_left_col_content') || get_field('lms_right_col_content') ) : //Check Title ?>
                    <div class="heartisense-lms-main-box">
                        <div class="cols cols2">
                            <?php if( have_rows('lms_left_col_images') || get_field('lms_left_col_content') ) : //Check Left Images Set or Content Set ?>
                                <div class="col">
                                    <div class="heartisense-lms-box">
                                        <?php if( have_rows('lms_left_col_images') ) : //Check Left Column Images Set ?>
                                            <div class="heartisense-slider">
                                            <?php while( have_rows('lms_left_col_images') ) : the_row(); //Loop to List Left Col Images
                                                $image = get_sub_field('image'); //Images ?>
                                                <div class="slide-item">
                                                    <a href="<?php echo $image['url'];?>" class="venoboxvid vbox-item" data-gall="gall-lms"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>"></a>
                                                </div><!--/.slide-item-->
                                            <?php endwhile; //Endwhile ?>
                                            </div><!--/.heartisense-slider-->
                                        <?php endif; //Endif
                                        if( get_field('lms_left_col_content') ) : //Check Left Content Set ?>
                                            <div class="heartisense-details-box">
                                                <?php the_field('lms_left_col_content'); //Left Content ?>
                                            </div><!--/.heartisense-details-box-->
                                        <?php endif; //Endif ?>
                                    </div><!--/.heartisense-lms-box-->
                                </div><!--/.col-->
                            <?php endif; //Endif
                            if( get_field('lms_right_col_content') ) : //Right Column Content ?>
                                <div class="col">
                                    <div class="heartisense-lms-box">
                                        <?php the_field('lms_right_col_content');?>
                                    </div><!--/.heartisense-lms-box-->
                                </div><!--/.col-->
                            <?php endif; //Endif ?>
                        </div><!--/.cols2-->
                    </div><!--/.heartisense-lms-main-box-->
                <?php endif; //Endif ?>
            </div><!--/.wrap-->
        </div><!--/.heartisense-structure-box-->

    <?php endif; //Endif

	//Home Page Testimonials
	get_template_part( 'page-contents/content', 'testimonials' );
	
	//Request a Quote Section
	get_template_part( 'page-contents/content', 'request-quote' );
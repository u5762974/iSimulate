<?php
/**
 * The template for displaying our product section
 *
 * This is the template that displays home page content
 *
 * @since iSimulate 1.0
 **/

	//This section display on homepage
	if( is_page_template( 'page-templates/template-home.php' ) ) : 
            // Hero Slider Section 
            if( have_rows( 'hero_slider' ) ): //To check slider images 
                $background = get_field( 'hero_background_image' ) ? ' style= "background-image: url('.get_field( 'hero_background_image' ).')"' :''; ?>
                <section class="hero-section"<?php echo $background; ?> >
                    <div class="hero-slider">
                            <?php while( have_rows( 'hero_slider' ) ) : the_row(); //Loop Start ?>
                            <div class="slide-item">
                                <div class="wrap">
                                    <div class="screen-size">
                                        <div class="banner-textbox">
                                            <?php if($banner_logo = get_sub_field( 'logo' )):?>
                                                    <img src="<?php echo $banner_logo['url']; ?>" alt="<?php bloginfo( 'name' );?>">
                                            <?php endif;
                                            if( get_sub_field( 'text' ) ):?>
                                                    <h2><?php the_sub_field( 'text' ); ?></h2>
                                            <?php endif;
                                            if( get_sub_field( 'read_more_link' ) || get_sub_field( 'request_a_quote_link' ) ): ?>
                                            <div class="button-row">
                                                <?php if( get_sub_field( 'read_more_link' ) ): // To add remore button link?>
                                                    <a href="<?php the_sub_field( 'read_more_link' ); ?>" class="button btn-outline readmore"><?php _e( 'Read More','isimulate'); ?></a>
                                                <?php endif;
                                                if( get_sub_field( 'request_a_quote_link' ) ): //To add request a quote button link?>
                                                        <a class="button btn-lg" href="<?php the_sub_field('request_a_quote_link'); ?>"><?php _e( 'Request a Quote','isimulate' );?></a>
                                                <?php endif; ?>
                                            </div>
                                            <?php endif;?>
                                        </div><!--/.banner-textbox -->
                                        <?php if( $product_banner = get_sub_field('product_image') ): //To retrive banner product image?>
                                            <div class="banner-imagebox">
                                                <img src="<?php echo $product_banner['url']; ?>" alt="<?php bloginfo( 'name' );?>">
                                            </div><!--/.banner-imagebox -->
                                        <?php endif; ?>
                                    </div><!--/.screen-size -->
                                </div><!--/.wrap -->
                            </div><!--/.slide-item -->
                        <?php endwhile;?>
                    </div><!--/.hero-slider -->
                    <a href="#main" class="backtobottom" title="backtobottom">Back to Bottom</a>
                </section><!--/.hero-section -->
            <?php endif;
        elseif( is_page_template( 'page-templates/template-coming-soon-product.php' ) ) : //header for comming soon page template
            // To retrive banner image
            $productbanner = get_field( 'product_banner',get_the_ID() ) ? get_field( 'product_banner',get_the_ID() ) : ISIMULATE_PAGE_BANNER; ?>
            <section class="product-banner" style="background-image: url('<?php echo $productbanner; ?>')" >
                <div class="wrap">
                    <div class="banner-top-section">
                        <div class="banner-text">
                            <?php if( $product_logo_image = get_field( 'upcoming_product_logo',get_the_ID() ) ):  //to add product logo in header part ?>
                                <div class="banner-logo-glow"><img src="<?php echo $product_logo_image['url']; ?>" alt="<?php echo get_the_title(); ?>"></div>
                            <?php endif;
                            if( get_field( 'upcoming_banner_title' ) ):?>
                                <h1><?php the_field( 'upcoming_banner_title' ); ?></h1>
                            <?php endif; ?>

                            <?php if( get_field('upcoming_button_text') ): //to check product banner button link
                                $button_text = get_field( 'upcoming_button_text' )? get_field( 'upcoming_button_text' ): __('Request a Quote','isimulate'); //To retrive button text ?>
                                <a href="<?php echo get_field('upcoming_button_url'); ?>" class="button btn-lg"><?php echo $button_text; ?></a>
                            <?php endif; ?>
                        </div><!--/.banner-text -->
                        
                        <?php //Countdown
                        if( get_field( 'upcoming_product_end_date' ) ):
                        $start_date = ( get_field( 'upcoming_product_start_date' ) ) ? strtotime( get_field( 'upcoming_product_start_date' ) ) : time();
                        $currentdate = time();
                        $enddate = strtotime( get_field( 'upcoming_product_end_date' ) );
                        $total_diff = floor( ( $enddate - $start_date ) / 86400 );
                        $daysdiff = floor( ( $enddate - $currentdate ) / 86400 );
                        $diff = $total_diff - $daysdiff;
                        $percentage = ceil( ($diff /$total_diff) * 100 ); ?>
                        <div class="circle-countdown">
                            <div class="loading-canvasbox">
                                <span class="percent-text"> <?php echo $percentage;?>%</span>
                                <input class="knob" data-width="294" data-height="294" data-readOnly=true data-fgColor="#fdd22b" data-bgColor="#ffffff" data-thickness=".45"  data-displayInput=false value="<?php echo $percentage;?>" style="visibility:hidden;">
                            </div>
                            <span class="loding-text">Loading...</span>
                        </div>
                        <?php endif;?>
                    </div><!--/.banner-top-section-->
                    <?php if( get_field( 'upcoming_page_title' ) ): ?>
                        <h2 class="available-info"><?php the_field( 'upcoming_page_title' ); ?></h2>
                    <?php endif; ?>
                </div><!--/.wrap-->
                
            </section><!--/.product-banner -->
	<?php elseif( is_singular( 'product' ) ): //to check product custom post type 
            // To retrive banner image
            $productbanner = get_field( 'product_banner',get_the_ID() ) ? get_field( 'product_banner',get_the_ID() ) : ISIMULATE_PAGE_BANNER; ?>

            <section class="product-banner" style="background-image: url('<?php echo $productbanner; ?>')" >
                <div class="wrap">
                    <div class="banner-text">
                        <?php if( $product_logo_image = get_field( 'product_logo',get_the_ID() ) ):  //to add product logo in header part ?>
                            <img src="<?php echo $product_logo_image['sizes']['product-logo']; ?>" alt="<?php echo get_the_title(); ?>">
                        <?php endif;

                        the_title('<h1>','</h1>'); // to retrive product title

                        if( get_field('product_banner_button_link') ): //to check product banner button link
                            $button_text = get_field( 'product_banner_button_text' )? get_field( 'product_banner_button_text' ): 'Request a Quote'; //To retrive button text ?>
                            <a href="<?php echo get_field('product_banner_button_link'); ?>" class="button btn-lg"><?php echo $button_text; ?></a>
                        <?php endif; ?>
                    </div><!--/.banner-text -->
                    <?php if( $product_image = get_field( 'product_image',get_the_ID() ) ):  //to add product logo in header part ?>
                        <div class="banner-image">
                            <img src="<?php echo $product_image['sizes']['product-banner-image']; ?>" alt="<?php echo get_the_title(); ?>">
                        </div><!--/.banner-image -->
                    <?php endif; ?>
                </div><!--/.wrap-->
            </section><!--/.product-banner -->
    <?php elseif ( is_author() ): //Author wise blog listing
    	$pagebanner = get_field( 'page_header_banner',ISIMULATE_BLOGPAGE_ID ) ? get_field( 'page_header_banner',get_the_ID() ):ISIMULATE_PAGE_BANNER;?>
		<div class="single-page-banner-box style= "background-image: url('<?php echo $page_banner;?>');" >
            <div class="wrap">
                <div class="page-title-box"><h3 class="page-title"><?php printf('Author : %s',get_the_author_meta('display_name'));; ?></h3></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
    <?php elseif ( is_category() ): //category wise blog listing
    	$pagebanner = get_field( 'page_header_banner',ISIMULATE_BLOGPAGE_ID ) ? get_field( 'page_header_banner',get_the_ID() ):ISIMULATE_PAGE_BANNER;?>
		<div class="single-page-banner-box style= "background-image: url('<?php echo $page_banner;?>');" >
            <div class="wrap">
                <div class="page-title-box"><h3 class="page-title"><?php single_cat_title('Category : '); ?></h3></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
    <?php elseif ( is_home() || is_singular('post')):

    	$pagebanner = get_field( 'page_header_banner',ISIMULATE_BLOGPAGE_ID ) ? get_field( 'page_header_banner',get_the_ID() ):ISIMULATE_PAGE_BANNER;?>
		<div class="single-page-banner-box style= "background-image: url('<?php echo $page_banner;?>');" >
            <div class="wrap">
                <div class="page-title-box"><h3 class="page-title"><?php echo get_the_title( ISIMULATE_BLOGPAGE_ID ); ?></h3></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
	<?php elseif( is_404() ): //If page is 404 page?>
        <div class="single-page-banner-box">
            <div class="wrap">
                <div class="page-title-box"><h3 class="page-title"><?php echo _e('404 Page Not Found','isimulate'); ?></h3></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
    <?php elseif( is_search() ): //If page is 404 page?>
        <div class="single-page-banner-box">
            <div class="wrap">
                <div class="page-title-box"><h3 class="page-title"><?php printf('%s : %s',__('Search','isimulate'),get_search_query()); ?></h3></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
	<?php else: //For page header other than home page 
		$pagebanner = get_field( 'page_header_banner',get_the_ID() ) ? ' style= "background-image: url('.get_field( 'page_header_banner',get_the_ID() ).')"' :ISIMULATE_PAGE_BANNER;?>
		<div class="single-page-banner-box"<?php echo $page_banner;?>>
            <div class="wrap">
                <div class="page-title-box"><?php the_title('<h3 class="page-title">','</h3>');?></div><!--/.page-title-box -->
            </div><!--/.wrap-->
        </div><!--/.single-page-banner-box-->
	<?php endif;?>
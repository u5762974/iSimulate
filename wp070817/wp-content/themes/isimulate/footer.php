<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>

	</div><!-- .main -->
    <footer id="footer">
    	<div class="wrap">
			<a title="backtotop" class="backtotop" href="#">Back to Top</a>
        	<div class="footer-logo-social">
    	       	<?php if( get_field( 'logo','option' ) ) : //check logo is set in options page or not ?>
        	        <a href="<?php bloginfo( 'url' );?>" id="footer-logo" title="<?php bloginfo( 'name' );?>"><img src="<?php the_field( 'logo','option' );?>" alt="<?php bloginfo( 'name' );?>" /></a>
	            <?php endif; ?>
                <div class="menu-social-links-container">
                	<?php wp_nav_menu( array( 'theme_location' => 'social', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
                </div>
            </div>
            
            <div class="fmenu-box">
            	<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '', 'items_wrap' => '<ul class="fmenu">%3$s</ul>' ) ); ?>
            	<div class="copy-right-box">
                	<?php if( get_field('copyright', 'option') ) : //Copyright set or not?>
                    <p class="copyright"><?php echo str_replace('[year]', date('Y'), get_field('copyright', 'option'));?></p><!--/.copyright -->
                    <?php endif; ?>
                </div><!--/.copy-right -->  
            </div><!--/.f-menu-box -->  
            
        </div><!--/.wrap -->
    </footer><!--/#footer -->
    <?php if( is_page_template( 'page-templates/template-your-story.php' ) ) : ?>
        <div class="popouterbox" id="edit-story">
            <div class="popup-block">
                <a href="#" class="close-dialogbox">close</a>
                <div class="pop-contentbox">
                    <h4 class="section-title">write your Story here</h4>
                    <div class="popup-details">
                    	<?php echo do_shortcode( '[gravityform id="2" title="false" description="false"]' );//Write your review form for your story page ?>
                    </div><!--/.popup-details-->
                </div><!--/.pop-contentbox-->
            </div><!--/.popup-block -->
        </div><!--/.popouterbox -->
    <?php endif; ?>
        
</div><!--/#wrapper -->

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * Theme AJAX
 *
 * Handles to total ajax of theme
 *
 * @since iSimulate 1.0
 **/

/**
 * Blog Index Load More
 *
 * Handles to load more blog posts
 * 
 * @since iSimulate 1.0
 **/
if( ! function_exists('isimulate_load_more_story') ) :
/**
 * Accomodation Popup AJAX Call
 * 
 * Handles to accomodation popup ajax call
 * 
 * @since Safarihub 1.0
 **/
function isimulate_load_more_story(){
    	//page number
	$pageno = isset( $_POST['paged'] ) && !empty( $_POST['paged'] ) ? intval( $_POST['paged'] ) : 1;
	
	//initial posts arguments
	$posts_args = array( 'post_type' => 'your-story', 'post_status' => 'publish', 'paged' => ( $pageno + 1 ) );
	
	//check is technology
	if( isset( $_POST['technology'] )	&&	!empty( $_POST['technology'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'group','field'=>'slug','terms'=> $_POST['technology']); 
	}//endif

	//check is country
	if( isset( $_POST['country'] )	&&	!empty( $_POST['country'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'country','field'=>'slug','terms'=> $_POST['country']); 
	}//endif
	
	//posts listing
	$posts_query = new WP_Query( $posts_args );
	
	//buffer start
	ob_start();
	//check posts data
	if( $posts_query->have_posts() ) : 
		
		//The Loop for blog lists
		while( $posts_query->have_posts() ) : $posts_query->the_post();
			
			//List Single Blog Block
			get_template_part('page-contents/content','your-story-box');
    
		//endwhile
		endwhile;
	//endif
	endif;
	
	//initial result
	$result = array();
	//HTML of all Blocks
	$result['html'] = ob_get_contents();
	//Clean Buffer
	ob_get_clean();
	$result['technology'] = $_POST['technology'];
	//Current Page
	$result['paged']	 =	$posts_query->query['paged'];
	//Check more page exist or not
	$result['more_page'] =	( $posts_query->max_num_pages == $result['paged'] ? 0 : 1 );
	
	//wp reset post data
	wp_reset_postdata();
	
	echo json_encode( $result );
	//Die for proper ajax response
	wp_die();
	
}
add_action('wp_ajax_isimulate_load_more_story',         'isimulate_load_more_story');
add_action('wp_ajax_nopriv_isimulate_load_more_story',  'isimulate_load_more_story');
endif;

if( ! function_exists('isimulate_technology_filter') ) :
/**
 * Technology filter AJAX Call
 * 
 * Handles technology filter on your story page
 * 
 * @since iSimulate 1.0
 **/
function isimulate_technology_filter(){

    //page number
	$pageno = isset( $_POST['paged'] ) && !empty( $_POST['paged'] ) ? intval( $_POST['paged'] ) : 1;
	
	//initial posts arguments
	$posts_args = array( 'post_type' => 'your-story', 'post_status' => 'publish','paged' => ( $pageno ) );
	//check is technology
	if( isset( $_POST['technology'] )	&&	!empty( $_POST['technology'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'group','field'=>'slug','terms'=> $_POST['technology']); 
	}//endif
	//check is country
	if( isset( $_POST['country'] )	&&	!empty( $_POST['country'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'country','field'=>'slug','terms'=> $_POST['country']); 
	}//endif
	//posts listing
	$posts_query = new WP_Query( $posts_args );
	
	//buffer start
	ob_start();
	//check posts data
	if( $posts_query->have_posts() ) : ?>
		<div class="story-list-box">
			<?php while( $posts_query->have_posts() ) : $posts_query->the_post(); //Loop start
                //request a quote section
                get_template_part( 'page-contents/content', 'your-story-box' );
            endwhile;?>
            <?php isimulate_load_more_button($posts_query);?>
        </div><!--/.story-list-box-->
	<?php
	else:
		get_template_part( 'page-contents/content', 'your-story-none' );
	//endif
	endif;
	
	//initial result
	$result = array();
	//HTML of all Blocks
	$result['html'] = ob_get_contents();
	//Clean Buffer
	ob_get_clean();
	$result['technology'] = $_POST['technology'];
	//Current Page
	$result['paged']	 =	1;
	//Check more page exist or not
	$result['more_page'] =	( $posts_query->max_num_pages == $result['paged'] ? 0 : 1 );
	
	//wp reset post data
	wp_reset_postdata();
	
	echo json_encode( $result );
	//Die for proper ajax response
	wp_die();
	
}
add_action('wp_ajax_isimulate_technology_filter',         'isimulate_technology_filter');
add_action('wp_ajax_nopriv_isimulate_technology_filter',  'isimulate_technology_filter');
endif;


if( ! function_exists('isimulate_country_filter') ) :
/**
 * Technology filter AJAX Call
 * 
 * Handles country filter on your story page
 * 
 * @since iSimulate 1.0
 **/
function isimulate_country_filter(){

    //page number
	$pageno = isset( $_POST['paged'] ) && !empty( $_POST['paged'] ) ? intval( $_POST['paged'] ) : 1;
	
	//initial posts arguments
	$posts_args = array( 'post_type' => 'your-story', 'post_status' => 'publish', 'paged' => ( $pageno) );
	//check is technology
	if( isset( $_POST['technology'] )	&&	!empty( $_POST['technology'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'group','field'=>'slug','terms'=> $_POST['technology']); 
	}//endif
	//check is country
	if( isset( $_POST['country'] )	&&	!empty( $_POST['country'] ) ) { 
		$posts_args['tax_query'][] = array('taxonomy'=>'country','field'=>'slug','terms'=> $_POST['country']); 
	}//endif
	//posts listing
	$posts_query = new WP_Query( $posts_args );
	
	//buffer start
	ob_start();
	//check posts data
	if( $posts_query->have_posts() ) : ?>
		<div class="story-list-box">
			<?php while( $posts_query->have_posts() ) : $posts_query->the_post(); //Loop start
                //request a quote section
                get_template_part( 'page-contents/content', 'your-story-box' );
            endwhile;?>
            <?php isimulate_load_more_button($posts_query);?>
        </div><!--/.story-list-box-->
	<?php
	else:
		get_template_part( 'page-contents/content', 'your-story-none' );
	//endif
	endif;
	
	//initial result
	$result = array();
	//HTML of all Blocks
	$result['html'] = ob_get_contents();
	//Clean Buffer
	ob_get_clean();
	//Current Page
	$result['paged']	 =	1;
	//Check more page exist or not
	$result['more_page'] =	( $posts_query->max_num_pages == $result['paged'] ? 0 : 1 );
	
	//wp reset post data
	wp_reset_postdata();
	
	echo json_encode( $result );
	//Die for proper ajax response
	wp_die();
	
}
add_action('wp_ajax_isimulate_country_filter',         'isimulate_country_filter');
add_action('wp_ajax_nopriv_isimulate_country_filter',  'isimulate_country_filter');
endif;
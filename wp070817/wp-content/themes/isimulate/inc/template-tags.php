<?php
/**
 * Custom template tags for iSimulate
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */

if ( ! function_exists( 'isimulate_comment_nav' ) ) :
/**
 * Display navigation to next/previous comments when applicable.
 *
 * @since iSimulate 1.0
 */
function isimulate_comment_nav() {
	// Are there comments to navigate through?
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
	?>
	<nav class="navigation comment-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Comment navigation', 'isimulate' ); ?></h2>
		<div class="nav-links">
			<?php
				if ( $prev_link = get_previous_comments_link( __( 'Older Comments', 'isimulate' ) ) ) :
					printf( '<div class="nav-previous">%s</div>', $prev_link );
				endif;

				if ( $next_link = get_next_comments_link( __( 'Newer Comments', 'isimulate' ) ) ) :
					printf( '<div class="nav-next">%s</div>', $next_link );
				endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .comment-navigation -->
	<?php
	endif;
}
endif;

if ( ! function_exists( 'isimulate_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * @since iSimulate 1.0
 */
function isimulate_entry_meta() {

	echo '<ul class="blog-article-details-share">';

	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) { // To retrive post date
		
		printf( '<li>%1$s <a href="%2$s">%3$s</a></li>',
			__( 'Date : ', 'isimulate' ),
			esc_url( get_permalink() ),
			get_the_date()
		);
	}

	if ( 'post' == get_post_type() ) {

			printf( '<li>%1$s <a href="%2$s">%3$s</a></li>',
				__( 'Posted By', 'isimulate' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author()
			);


		$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'isimulate' ) );
		if ( $categories_list ) {
			printf( '<li>%1$s %2$s</li>',
				__( 'Categories :','isimulate' ),
				$categories_list
			);
		}

		/*$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'isimulate' ) );
		if ( $tags_list ) {
			printf( '<li>%1$s %2$s</li>',
				__( 'Tags :', 'isimulate' ),
				$tags_list
			);
		}*/
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		printf( '<li>%1$s %2$s</li>',
				__( 'Comments :', 'isimulate' ),
				get_comments_number()
			);
	}
	echo '</ul>';
}
endif;

/**
 * Determine whether blog/site has more than one category.
 *
 * @since iSimulate 1.0
 *
 * @return bool True of there is more than one category, false otherwise.
 */
function isimulate_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'isimulate_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'isimulate_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so isimulate_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so isimulate_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in {@see isimulate_categorized_blog()}.
 *
 * @since iSimulate 1.0
 */
function isimulate_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'isimulate_categories' );
}
add_action( 'edit_category', 'isimulate_category_transient_flusher' );
add_action( 'save_post',     'isimulate_category_transient_flusher' );

if ( ! function_exists( 'isimulate_post_thumbnail' ) ) :
/**
 * Display an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 *
 * @since iSimulate 1.0
 */
function isimulate_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		?>
	</a>

	<?php endif; // End is_singular()
}
endif;

if ( ! function_exists( 'isimulate_get_link_url' ) ) :
/**
 * Return the post URL.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since iSimulate 1.0
 *
 * @see get_url_in_content()
 *
 * @return string The Link format URL.
 */
function isimulate_get_link_url() {
	$has_url = get_url_in_content( get_the_content() );

	return $has_url ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}
endif;

if ( ! function_exists( 'isimulate_excerpt_more' ) && ! is_admin() ) :
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and a 'Continue reading' link.
 *
 * @since iSimulate 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function isimulate_excerpt_more( $more ) {
	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading %s', 'isimulate' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
		);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'isimulate_excerpt_more' );
endif;

if ( ! function_exists( 'isimulate_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since iSimulate 1.5
 */
function isimulate_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;
if ( ! function_exists( 'isimulate_load_more_button' ) ) :
/**
 * Show Load More News Button
 * 
 * Handles to show load more news button
 *
 * @since iSimulate 1.0
 **/
function isimulate_load_more_button( $query = null){
	
	global $wp_query;
	
	//check query empty
	if( empty($query) ){
		$query = $wp_query;
	}
	//check paged greater than max number pages
	$paged = get_query_var('paged' ) ? get_query_var('paged' ) : 1;
	if( $query->max_num_pages > $paged ) :
		//query data 
		$querydata = array();
		//check page template is 
		$querydata[] = 'data-only-story="true"'; 
?>      
        <div class="load-more-wrap">
            <a href="javascript:void(0);" class="button btn-lg load-story" data-paged="<?php echo $paged;?>" <?php echo join(' ',$querydata);?>><span><?php _e('Load More Stories','isimulate');?></span></a>
            <img src="<?php echo ISIMULATE_THEME_URL;?>/images/ajax-loader.gif" alt="<?php _e('Loading...','isimulate');?>" class="ajax-loader"/>
        </div><!--/.load-more-wrap-->
<?php endif; //endif 
}
endif;

function isimulate_post_search_only($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
add_filter('pre_get_posts','isimulate_post_search_only');


//Add custom select box in contact form 7 (retrive dynamic value from texonomy)
//[select name term:taxonomy_name]
function isimulate_dynamic_select_list($tag, $unused){ 
    $options = (array)$tag['options'];

    foreach ($options as $option) 
        if (preg_match('%^term:([-0-9a-zA-Z_]+)$%', $option, $matches)) 
            $term = $matches[1];

    //check if post_type is set
    if(!isset($term))
        return $tag;

    $taxonomy = get_terms($term, array('hide_empty' => 0));

    if (!$taxonomy)  
        return $tag;

    foreach ($taxonomy as $cat) {  
        $tag['raw_values'][] = $cat->name;  
        $tag['values'][] = $cat->name;  
        $tag['labels'][] = $cat->name;
    }
    return $tag; 
}
add_filter( 'wpcf7_form_tag', 'isimulate_dynamic_select_list', 10, 2);

<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Theme Shortcodes
 *
 * Handles to convert shortcode to its content
 *
 * @since iSimulate 1.0
 **/
 
if( ! function_exists('isimulate_cols_shortcodes') ) :
/**
 * Columns Shortcodes
 *
 * Handles to render columns shortcodes
 *
 * @since iSimulate 1.0
 **/
function isimulate_cols_shortcodes($atts, $content){
	$atts = extract( shortcode_atts( array(
		'class' => '',
	), $atts, 'cols' ) );
	$class= ($class) ? ' '.$class: ' cols2';
	//return shortcode content
	return '<div class="cols'.$class.'">'.do_shortcode($content).'</div><!--.col-->';
}
add_shortcode('cols', 'isimulate_cols_shortcodes');
endif;

if( ! function_exists('isimulate_col_shortcodes') ) :
/**
 * Columns Shortcodes
 *
 * Handles to render columns shortcodes
 *
 * @since iSimulate 1.0
 **/
function isimulate_col_shortcodes($atts, $content){
	//return shortcode content
	return '<div class="col">'.do_shortcode($content).'</div><!--.col-->';
}
add_shortcode('col', 'isimulate_col_shortcodes');
endif;

if( ! function_exists('isimulate_ul_customlist_shortcodes') ) :
/**
 * ul customlist shortcodes
 *
 * Handles to render ul class custom list shortcodes
 *
 * @since iSimulate 1.0
 **/
function isimulate_ul_customlist_shortcodes($atts, $content){
	$atts = extract( shortcode_atts( array(
		'class' => '',
		'title' => '',
	), $atts, 'customlist' ) );
	$class=!empty($class) ? $class : 'custom-arrow-list'; 
	$title=!empty($title) ? '<h5>'.$title.'</h5>' : ''; 
	return $title.'<ul class="'.$class.'">'.do_shortcode($content).'</ul>';
}
add_shortcode('customlist', 'isimulate_ul_customlist_shortcodes');
endif;

if( ! function_exists('isimulate_li_customlist_shortcodes') ) :
/**
 * li customlist shortcodes
 *
 * Handles to render ul class custom list li shortcodes
 *
 * @since iSimulate 1.0
 **/
function isimulate_li_customlist_shortcodes($atts, $content){

	return '<li>'.do_shortcode($content).'</li>';

}
add_shortcode('li', 'isimulate_li_customlist_shortcodes');
endif;
<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;
/**
 * Products Custom Post Type
 *
 * Handles to register custom post type for videos
 *
 * @since iSimulate 1.0
 **/
if( !function_exists('isimulate_register_product_posts') ) :
/**
 * Register Product Posts
 *
 * Handles to register product post type
 *
 * @since iSimulate 1.0
 **/
function isimulate_register_product_posts(){

	//product labels
	$productlabels = array(
		'name'				=>	_x( 'Products', 'post type general name', 'isimulate' ),
		'singular_name'		=>	_x( 'Product', 'post type singular name', 'isimulate' ),
		'menu_name'			=>	_x( 'Products', 'admin menu', 'isimulate' ),
		'name_admin_bar'	=>	_x( 'Product', 'add new on admin bar', 'isimulate' ),
		'add_new'			=>	_x( 'Add New', 'product', 'isimulate' ),
		'add_new_item'		=>	__( 'Add New', 'isimulate' ),
		'new_item'			=>	__( 'New Product', 'isimulate' ),
		'edit_item'			=>	__( 'Edit Product', 'isimulate' ),
		'view_item'			=>	__( 'View Product', 'isimulate' ),
		'all_items'			=>	__( 'All Products', 'isimulate' ),
		'search_items'		=>	__( 'Search', 'isimulate' ),
		'parent_item_colon'	=>	__( 'Parent Products:', 'isimulate' ),
		'not_found'			=>	__( 'No product found.', 'isimulate' ),
		'not_found_in_trash'=>	__( 'No product found in Trash.', 'isimulate' )
	);
	
	//product argument
	$productargs = array(
		'labels'			=>	$productlabels,
		'description'		=>	__('To show products.', 'isimulate'),
		'public'			=>	true,
		'publicly_queryable'=>	true,
		'show_ui'			=>	true,
		'show_in_menu'		=>	true,
		'query_var'			=>	true,
		'rewrite'			=>	array('slug' => 'product'),
		'capability_type'	=>	'post',
		'has_archive'		=>	false,
		'hierarchical'		=>	false,
		'menu_position'		=>	null,
		'menu_icon'			=>	'dashicons-tickets-alt',
		'supports'			=>	array('title', 'editor', 'thumbnail')
	);
	//register post type
	register_post_type('product', $productargs);
}
add_action('init','isimulate_register_product_posts');
endif;
if( !function_exists('isimulate_product_updated_messages') ) :
/**
 * Update Messages
 *
 * Handles to update messages
 *
 * @since iSimulate 1.0
 **/
function isimulate_product_updated_messages( $messages ){
	
	$post             = get_post();
	$post_type        = get_post_type( $post );
	$post_type_object = get_post_type_object( $post_type );

	$messages['product'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => __( 'Product updated.', 'isimulate' ),
		2  => __( 'Custom field updated.', 'isimulate' ),
		3  => __( 'Custom field deleted.', 'isimulate' ),
		4  => __( 'Product updated.', 'isimulate' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Product restored to revision from %s', 'isimulate' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => __( 'Product published.', 'isimulate' ),
		7  => __( 'Product saved.', 'isimulate' ),
		8  => __( 'Product submitted.', 'isimulate' ),
		9  => sprintf( __( 'Product scheduled for: <strong>%1$s</strong>.', 'isimulate' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i', 'isimulate' ), strtotime( $post->post_date ) )
		),
		10 => __( 'Product draft updated.', 'isimulate' )
	);

	if ( $post_type_object->publicly_queryable && $post_type == 'product' ) {

		$permalink = get_permalink( $post->ID );
		$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View Product', 'isimulate' ) );
		$messages[ $post_type ][1] .= $view_link;
		$messages[ $post_type ][6] .= $view_link;
		$messages[ $post_type ][9] .= $view_link;

		$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
		$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview Product', 'isimulate' ) );
		$messages[ $post_type ][8]  .= $preview_link;
		$messages[ $post_type ][10] .= $preview_link;
	}
	//return new messages
	return $messages;
}
add_filter('post_updated_messages', 'isimulate_product_updated_messages');
endif;
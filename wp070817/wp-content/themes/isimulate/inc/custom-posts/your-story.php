<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;
/**
 * Your Story Custom Post Type
 *
 * Handles to register custom post type for your story
 *
 * @since iSimulate 1.0
 **/
if( !function_exists('isimulate_register_your_story_posts') ) :

/**
 * Register Your Story Posts
 *
 * Handles to register your story post type
 *
 * @since iSimulate 1.0
 **/
function isimulate_register_your_story_posts(){
	
	//package labels
	$yourstoryabels = array(
		'name'				=>	_x('Your Stories', 'post type general name', 'isimulate'),
		'singular_name'		=>	_x('Your Story', 'post type singular name', 'isimulate'),
		'menu_name'			=>	_x('Your Stories', 'admin menu', 'isimulate'),
		'name_admin_bar'	=>	_x('Your Stories', 'add new on admin bar', 'isimulate'),
		'add_new'			=>	_x('Add New', 'your-story', 'isimulate'),
		'add_new_item'		=>	__('Add New', 'isimulate'),
		'new_item'			=>	__('New Story', 'isimulate'),
		'edit_item'			=>	__('Edit Story', 'isimulate'),
		'view_item'			=>	__('View Story', 'isimulate'),
		'all_items'			=>	__('All Story', 'isimulate'),
		'search_items'		=>	__('Search Story', 'isimulate'),
		'parent_item_colon'	=>	__('Parent Story:', 'isimulate'),
		'not_found'			=>	__('No story found.', 'isimulate'),
		'not_found_in_trash'=>	__('No story found in Trash.', 'isimulate')
	);

	//your story argument
	$yourstoryargs = array(
		'labels'			=>	$yourstoryabels,
		'description'		=>	__('To show story.', 'isimulate'),
		'public'			=>	true,
		'publicly_queryable'=>	true,
		'show_ui'			=>	true,
		'show_in_menu'		=>	true,
		'query_var'			=>	true,
		'rewrite'			=>	array('slug' => 'your-story'),
		'capability_type'	=>	'post',
		'has_archive'		=>	false,
		'hierarchical'		=>	false,
		'menu_position'		=>	null,
		'menu_icon'			=>	'dashicons-book',
		'supports'			=>	array( 'title', 'editor', 'thumbnail', 'tags' )
	);
	//register post type
	register_post_type( 'your-story', $yourstoryargs );
}
add_action('init','isimulate_register_your_story_posts');
endif;
if( !function_exists('isimulate_your_story_updated_messages') ) :
/**
 * Update Messages
 *
 * Handles to update messages
 *
 * @since iSimulate 1.0
 **/
function isimulate_your_story_updated_messages( $messages ){
	
	$post             = get_post();
	$post_type        = get_post_type( $post );
	$post_type_object = get_post_type_object( $post_type );

	$messages['your-story'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => __( 'Story updated.', 'isimulate' ),
		2  => __( 'Custom field updated.', 'isimulate' ),
		3  => __( 'Custom field deleted.', 'isimulate' ),
		4  => __( 'Package updated.', 'isimulate' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Package restored to revision from %s', 'isimulate' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => __( 'Story published.', 'isimulate' ),
		7  => __( 'Story saved.', 'isimulate' ),
		8  => __( 'Story submitted.', 'isimulate' ),
		9  => sprintf( __( 'Story scheduled for: <strong>%1$s</strong>.', 'isimulate' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'M j, Y @ G:i', 'isimulate' ), strtotime( $post->post_date ) )
		),
		10 => __( 'Story draft updated.', 'isimulate' )
	);

	if ( $post_type_object->publicly_queryable && $post_type == 'your-story' ) {
		
		$permalink = get_permalink( $post->ID );
		$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View Story', 'isimulate' ) );
		$messages[ $post_type ][1] .= $view_link;
		$messages[ $post_type ][6] .= $view_link;
		$messages[ $post_type ][9] .= $view_link;

		$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
		$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview Story', 'isimulate' ) );
		$messages[ $post_type ][8]  .= $preview_link;
		$messages[ $post_type ][10] .= $preview_link;
	}
	//return new messages
	return $messages;
}
add_filter( 'post_updated_messages', 'isimulate_your_story_updated_messages' );
endif;

if( !function_exists('isimulate_your_story_taxonomies') ) :
/**
 * Register Your Story Taxonomies
 * 
 * Handles to register taxonomy
 * 
 * @package iSimulate 1.0
 */
function isimulate_your_story_taxonomies() {
    
    //Technology labels
	$technologieslabels = array(
		'name'              => _x('Group', 'taxonomy general name', 'isimulate'),
		'singular_name'     => _x('Group', 'taxonomy singular name', 'isimulate'),
		'search_items'      => __('Search Group', 'isimulate'),
		'all_items'         => __('All Group', 'isimulate'),
		'parent_item'       => __('Parent Group', 'isimulate'),
		'parent_item_colon' => __('Parent Group:', 'isimulate'),
		'edit_item'         => __('Edit Group', 'isimulate'),
		'update_item'       => __('Update Group', 'isimulate'),
		'add_new_item'      => __('Add New Group', 'isimulate'),
		'new_item_name'     => __('New Group Name', 'isimulate'),
		'menu_name'         => __('Groups', 'isimulate'),
		'not_found'			=>	__('No group found.', 'isimulate'),
		'not_found_in_trash'=>	__('No Group found in Trash.', 'isimulate')
	);
	//technology arguments
	$techologiesargs = array(
		'hierarchical'  =>  true,
		'labels'        =>  $technologieslabels,
		'show_ui'       =>  true,
		'show_admin_column'=> true,
		'query_var'     =>  true,
		'rewrite'       =>  array('slug' => 'your-story/group', 'hierarchical' => true, 'with_front' => false )
	);
	//register technology
	register_taxonomy( 'group', array( 'your-story' ), $techologiesargs );

	 //country labels
	$countrieslabels = array(
		'name'              => _x('Country', 'taxonomy general name', 'isimulate'),
		'singular_name'     => _x('Country', 'taxonomy singular name', 'isimulate'),
		'search_items'      => __('Search Countries', 'isimulate'),
		'all_items'         => __('All Countries', 'isimulate'),
		'parent_item'       => __('Parent Country', 'isimulate'),
		'parent_item_colon' => __('Parent Country:', 'isimulate'),
		'edit_item'         => __('Edit Country', 'isimulate'),
		'update_item'       => __('Update Country', 'isimulate'),
		'add_new_item'      => __('Add New Coutry', 'isimulate'),
		'new_item_name'     => __('New Country Name', 'isimulate'),
		'menu_name'         => __('Countries', 'isimulate'),
		'not_found'			=>	__('No country found.', 'isimulate'),
		'not_found_in_trash'=>	__('No country found in Trash.', 'isimulate')
	); 
	//Country arguments
	$countriesargs = array(
		'hierarchical'  =>  true,
		'labels'        =>  $countrieslabels,
		'show_ui'       =>  true,
		'show_admin_column'=> true,
		'query_var'     =>  true,
		'rewrite'       =>  array('slug' => 'your-story/country', 'hierarchical' => true, 'with_front' => false )
	);
	//register technology
	register_taxonomy( 'country', array( 'your-story' ), $countriesargs );
  
}
add_action( 'init', 'isimulate_your_story_taxonomies', 0 );
endif;

if( !function_exists('isimulate_technology_taxonomy_messages' ) ) :
/**
 * Taxonomy Update Messages
 * 
 * Handles to override taxonomy update messages
 * 
 * @since iSimulate 1.0
 **/
function isimulate_technology_taxonomy_messages( $messages ){
    
    //override taxonomy categories messages
    $messages['technology'] = array(
        0 => '',
        1 => __( 'Technology added.', 'isimulate' ),
        2 => __( 'Technology deleted.', 'isimulate' ),
        3 => __( 'Technology updated.', 'isimulate' ),
        4 => __( 'Technology not added.', 'isimulate' ),
        5 => __( 'Technology not updated.', 'isimulate' ),
        6 => __( 'Technologies deleted.', 'isimulate' )
    );

    //return messages
    return $messages;
}
add_filter('term_updated_messages', 'isimulate_technology_taxonomy_messages');
endif;
if( !function_exists('isimulate_country_taxonomy_messages' ) ) :
/**
 * Taxonomy Update Messages
 * 
 * Handles to override taxonomy update messages
 * 
 * @since iSimulate 1.0
 **/
function isimulate_country_taxonomy_messages( $messages ){
    
    //override taxonomy country messages
    $messages['country'] = array(
        0 => '',
        1 => __( 'Country added.', 'isimulate' ),
        2 => __( 'Country deleted.', 'isimulate' ),
        3 => __( 'Country updated.', 'isimulate' ),
        4 => __( 'Country not added.', 'isimulate' ),
        5 => __( 'Country not updated.', 'isimulate' ),
        6 => __( 'Countries deleted.', 'isimulate' )
    );

    //return messages
    return $messages;
}
add_filter('term_updated_messages', 'isimulate_country_taxonomy_messages');
endif;
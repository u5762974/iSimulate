<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Contact Page Template
 * 
 * Handles to show contact page content from this template
 *
 * @since iSimulate 1.0
 **/
get_header();?>
	<div id="primary" class="content-area one-column">
            <?php // Start the loop.
            while ( have_posts() ) : the_post();
            
                // Include the page content template.
                get_template_part( 'page-contents/content', 'contact' );
            
            // End the loop.
            endwhile; ?>
            <div id="contact-map"></div>
   	</div><!--/.content-area-->
<?php get_footer(); ?>
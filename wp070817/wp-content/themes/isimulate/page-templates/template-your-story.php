<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Your Story Page Template
 * 
 * Handles to show your story page content from this template
 *
 * @since iSimulate 1.0
 **/
get_header();?>
	<div id="primary" class="content-area one-column">
		<?php // Start the loop.
        while ( have_posts() ) : the_post();
            // Include the page content template.
            get_template_part( 'page-contents/content', 'your-story' );
        // End the loop.
        endwhile; ?>
   	</div><!--/.content-area-->
<?php get_footer(); ?>
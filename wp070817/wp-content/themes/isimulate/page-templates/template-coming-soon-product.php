<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Coming Soon Product Template
 * 
 * Handles to show home page content from this template
 *
 * @since iSimulate 1.0
 **/
get_header(); ?>

	<div id="primary" class="content-area one-column">

            <?php // Start the loop.
                    while ( have_posts() ) : the_post();

                            // Include the page content template.
                            get_template_part( 'page-contents/content', 'coming-soon-product' );

                    // End the loop.
                    endwhile; ?>

	</div>

<?php get_footer(); ?>
<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Distributors Page Template
 * 
 * Handles to show distributors page content from this template
 *
 * @since iSimulate 1.0
 **/
get_header();?>
	<div id="primary" class="content-area one-column">
    	<div class="distributors-list-main-box">
		<?php // Start the loop.
        while ( have_posts() ) : the_post();

            // Include the page content template.
            get_template_part( 'page-contents/content', 'distributors' );
        
        // End the loop.
        endwhile; ?>
        </div><!--/.distributors-list-main-box-->
   	</div><!--/.content-area-->
<?php get_footer(); ?>
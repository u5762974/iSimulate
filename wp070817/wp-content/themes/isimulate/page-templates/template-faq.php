<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: FAQ Page Template
 * 
 * Handles to show faq page content from this template
 *
 * @since iSimulate 1.0
 **/
get_header();?>
	<div id="primary" class="content-area one-column">
		<div class="wrap">
            <?php // Start the loop.
            while ( have_posts() ) : the_post();
            
                // Include the page content template.
                get_template_part( 'page-contents/content', 'faq' );
            
            // End the loop.
            endwhile; ?>
		</div><!--/.wrap-->
   	</div><!--/.content-area-->
<?php get_footer(); ?>
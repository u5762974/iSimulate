<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */
?>

<div class="blog-details-main-box">
	<div class="blog-list-item-details-box">
    	<div class="blog-details-box">
        	<?php if( has_post_thumbnail() ) : //Check Has Post Thumbnail?>
            <figure>
                <?php the_post_thumbnail('full', array( 'alt' => get_the_title() ) );?>
            </figure><!--/.post-image-->
		<?php endif;?>
		<?php the_content(); ?>
		</div><!-- .blog-details-box -->
    </div><!--/.blog-list-item-detail-box-->
</div><!--/.blog-detail-main-box-->
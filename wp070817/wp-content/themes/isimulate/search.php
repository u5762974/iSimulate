<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */

get_header(); ?>

	<div class="wrap">
		<div id="primary" class="content-area blog-page">

		<?php if ( have_posts() ) :

			// Start the loop.
			while ( have_posts() ) : the_post(); ?>

				<?php
				/*
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'page-contents/content', 'blog-list' );

			// End the loop.
			endwhile;

			wp_pagenavi(); //wp-pagenavi pagination

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</div><!--/#primary-->
        <?php get_sidebar();?>
  	</div><!--/wrap-->

<?php get_footer(); ?>

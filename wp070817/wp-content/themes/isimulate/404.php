<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage iSimulate
 * @since iSimulate 1.0
 */

get_header(); ?>

	<div class="wrap">
		<div id="primary" class="content-area one-column">
        
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'isimulate' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'isimulate' ); ?></p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</div><!-- #primary -->
	</div><!-- .wrap -->

<?php get_footer(); ?>

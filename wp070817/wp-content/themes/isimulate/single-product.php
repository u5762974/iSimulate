<?php
/**
 * The template for displaying all single product and attachments
 *
 */

get_header(); ?>

	<div id="primary" class="content-area one-column">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
                        if( get_the_ID() == ISIMULATE_HEARTISENSE_PAGE ) { //Check Current Product is HeartiSense
                            get_template_part( 'page-contents/content', 'product-heartisense' );
                        } else {
                            get_template_part( 'page-contents/content', 'product' );
                        }

		// End the loop.
		endwhile;
		?>

	</div><!-- .content-area -->

<?php get_footer(); ?>
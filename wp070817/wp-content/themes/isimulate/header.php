<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @since iSimulate 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]><html <?php language_attributes(); ?> class="ie7 ie no-js"> <![endif]-->
<!--[if IE 8]><html <?php language_attributes(); ?> class="ie8 ie no-js"> <![endif]-->
<!--[if IE 9]><html <?php language_attributes(); ?> class="ie9 ie no-js"> <![endif]-->
<!--[if gt IE 9]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="designer" content="pixlogix.com - a creative web design & development solutions">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
        <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<header id="header">
        <div class="wrap">
            <?php if( get_field( 'logo','option' ) ) : //check logo is set in options page or not ?>
                <a href="<?php bloginfo( 'url' );?>" id="logo" title="<?php bloginfo( 'name' );?>"><img src="<?php the_field( 'logo','option' );?>" alt="<?php bloginfo( 'name' );?>" /></a>
            <?php else : ?>
                <h1><a href="<?php bloginfo( 'url' );?>" id="logo" title="<?php bloginfo( 'name' );?>"><?php bloginfo( 'name' );?></a></h1>
            <?php endif; ?>
            <div class="header-menu">
            	<?php echo do_shortcode('[GTranslate] ');?>
            	<?php /*<div class="select-language" id="google_translate_element"></div>	<!--/.select-language-->*/?>
                <div id="menu-trigger" class="nav-menu">
                    <span class="menulines"></span>
                    <em class="nav-button"><?php _e('Menu','isimulate'); ?></em>                    
                </div> <!--/.nav-menu-->
        	</div><!--/.header-menu-->
        </div><!--/.wrap -->
        <nav id="main-nav" class="menu-box">
        	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'items_wrap' => '<ul class="clearfix" id="options">%3$s</ul>' ) ); ?>
        </nav><!--/#main-nav -->
	</header><!--/#header -->
	<?php get_template_part( 'page-contents/content', 'custom-header' ); //Custom header ?>
	<div id="main">
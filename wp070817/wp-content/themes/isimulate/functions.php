<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;
//Check Theme URL Defined
if( !defined('ISIMULATE_THEME_URL') ){
	define('ISIMULATE_THEME_URL', get_template_directory_uri());
}
//Check Theme Directory Defined
if( !defined('ISIMULATE_THEME_DIR') ){
	define('ISIMULATE_THEME_DIR', get_template_directory());
}

//define blog page id 
if( !defined('ISIMULATE_BLOGPAGE_ID') ){
	define('ISIMULATE_BLOGPAGE_ID', 165);
}

//define Product page id 
if( !defined('ISIMULATE_PRODUCT_PAGE') ){
	define('ISIMULATE_PRODUCT_PAGE', 165);
}

//define Distributor page id 
if( !defined('ISIMULATE_DISTRIBUTOR_PAGE') ){
	define('ISIMULATE_DISTRIBUTOR_PAGE', 180);
}

//define Distributor page id 
if( !defined('ISIMULATE_HEARTISENSE_PAGE') ){
	define('ISIMULATE_HEARTISENSE_PAGE', 303);
}

if( !defined('ISIMULATE_DEFAULT_PRODUCT_THUMB') ){
	define('ISIMULATE_DEFAULT_PRODUCT_THUMB', ISIMULATE_THEME_URL.'/images/default-295x196.png');
}

if( !defined('ISIMULATE_DEFAULT_VIDEO_THUMB') ){
	define('ISIMULATE_DEFAULT_VIDEO_THUMB', ISIMULATE_THEME_URL.'/images/default-338x190.png');
}

if( !defined('ISIMULATE_ADDON_DEFAULT_IMAGE') ){
	define('ISIMULATE_ADDON_DEFAULT_IMAGE', ISIMULATE_THEME_URL.'/images/default-338x259.png');
}

//Default image path for page banner
if( !defined('ISIMULATE_PAGE_BANNER') ){
	define('ISIMULATE_PAGE_BANNER', ISIMULATE_THEME_URL.'/images/bgi/product-banner.png');
}
//Default image path for resource feature image
if( !defined('ISIMULATE_RESOURCE_FEATURE_DEFAUL_IMAGE') ){
	define('RESOURCE_FEATURE_DEFAUL_IMAGE', ISIMULATE_THEME_URL.'/images/main-feature_default.jpg');
}

if ( ! function_exists( 'isimulate_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since iSimulate 1.0
 */
function isimulate_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on isimulate, use a find and replace
	 * to change 'isimulate' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'isimulate', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	//add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'isimulate' ),
		'social'  => __( 'Social Links Menu', 'isimulate' ),
		'footer-menu' => __( 'Footer Menu',      'isimulate' ),
	) );

	/*
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array('search-form') );

	
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css' ) );
}
endif; // isimulate_setup
add_action( 'after_setup_theme', 'isimulate_setup' );

/**
 * Register widget area.
 *
 * @since iSimulate 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function isimulate_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'isimulate' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'isimulate' ),
		'before_widget' => '<div id="%1$s" class="sidebar-block %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'isimulate_widgets_init' );

/**
 * Enqueue scripts and styles.
 *
 * @since iSimulate 1.0
 */
function isimulate_scripts() {

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'isimulate-style', ISIMULATE_THEME_URL . '/css/style.css' );
	
	// Load owl carousel stylesheet
	wp_enqueue_style( 'isimulate-owl-carousel', ISIMULATE_THEME_URL . '/css/vendor/owl.carousel.css' );
	
	// Load venobox stylesheet
	wp_enqueue_style( 'isimulate-venobox', ISIMULATE_THEME_URL . '/css/vendor/venobox.css' );
	
	// Load venobox stylesheet
	wp_enqueue_style( 'isimulate-responsive', ISIMULATE_THEME_URL . '/css/responsive.css' );
	
	//Load mordenizr script for HTML5 support
	wp_enqueue_script( 'isimulate-modernizr-script',  	ISIMULATE_THEME_URL . '/js/vendor/modernizr.min.js', array('jquery'), null, false );
	
	//Load jQuery
	wp_enqueue_script( array( 'jquery' ) );

	//Load owl carousel script
	wp_enqueue_script( 'isimulate-owl-carousel-script',  	ISIMULATE_THEME_URL . '/js/vendor/owl.carousel.min.js', array('jquery'), null, true );
	
	//Load venobox script
	wp_enqueue_script( 'isimulate-venobox-script',  	ISIMULATE_THEME_URL . '/js/vendor/venobox.js', array('jquery'), null, true );
	
	//Load ezmark script
	wp_enqueue_script( 'isimulate-ezmark-script',  	ISIMULATE_THEME_URL . '/js/vendor/jquery.ezmark.min.js', array('jquery'), null, true );
	
	//Load match height script
	wp_enqueue_script( 'isimulate-match-height-script',  	ISIMULATE_THEME_URL . '/js/vendor/jquery.matchHeight-min.js', array('jquery'), null, false );
        
        //Load match height script
	wp_enqueue_script( 'isimulate-knob-script',  	ISIMULATE_THEME_URL . '/js/vendor/jquery.knob.min.js', array('jquery'), null, true );
	
	if( is_page_template( 'page-templates/template-contact.php' ) ) :
		//google map jquery library
		wp_enqueue_script( 'isimulate-googlemap-script','//maps.google.com/maps/api/js?key=AIzaSyBsyHSp383z9Wh35L8UjSWfDzi5oiCoj68', array(), null, true );
	endif;
		
	//Load general script
	wp_enqueue_script( 'isimulate-general-script',  	ISIMULATE_THEME_URL . '/js/general.js', array('jquery'), null, true );
	$localize_genrs = array( 'imgurl' => ISIMULATE_THEME_URL . '/images' );

	//retriving langitute and latitude for google map on contact us page
	if( is_page_template( 'page-templates/template-contact.php' ) ) :
		//To retrive office address for google map
		if( have_rows('office_details') ): //To office addresses for google map
			$i=0;
			while ( have_rows('office_details') ) : the_row();
                                if(get_sub_field('title')):
                                    $arr['office_address_title'][] = get_sub_field('title');
                                    $arrGoogleMap = get_sub_field('google_map');
                                    $arr['office_address_lat'][] = $arrGoogleMap['lat'];
                                    $arr['office_address_lng'][] = $arrGoogleMap['lng'];
                                endif; 
			endwhile;
		endif;
		$localize_genrs['office_address']['title'] = $arr['office_address_title'];
		$localize_genrs['office_address']['lat'] = $arr['office_address_lat'];
		$localize_genrs['office_address']['lng']= $arr['office_address_lng'];

		//To retrive office address for google map
		if( have_rows('distributors',ISIMULATE_DISTRIBUTOR_PAGE ) ): //To office addresses for google map
			$i=0;
			while ( have_rows('distributors',ISIMULATE_DISTRIBUTOR_PAGE) ) : the_row();
				if($arrGoogleMap = get_sub_field('google_map')):
                                        if( get_sub_field('company_name') ):
                                            $arrDis['distributors_title'][] = get_sub_field('company_name');
                                            $arrDis['distributors_lat'][] = $arrGoogleMap['lat'];
                                            $arrDis['distributors_lng'][] = $arrGoogleMap['lng'];
                                        endif;
				endif;
			endwhile;
		endif;
		$localize_genrs['distributor_address']['title'] = $arrDis['distributors_title'];
		$localize_genrs['distributor_address']['lat'] = $arrDis['distributors_lat'];
		$localize_genrs['distributor_address']['lng']= $arrDis['distributors_lng'];
		//$localize_genrs['lat'] = $contact_map['lat'];
		//$localize_genrs['lng'] = $contact_map['lng'];
	endif;											
	wp_localize_script( 'isimulate-general-script',	'Isimulate_Genrs',	$localize_genrs );
	
	//Public Script
	wp_enqueue_script( 'isimulate-public-script',   ISIMULATE_THEME_URL . '/js/script-public.js', array('jquery'), null, true );
	wp_localize_script( 'isimulate-public-script', 'Isimulate_Obj',    array( 'ajaxurl' => admin_url('admin-ajax.php', is_ssl() ? 'https' : 'http' ) ) );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'isimulate-ie', get_template_directory_uri() . '/css/ie.css', array( 'isimulate-style' ), false );
	wp_style_add_data( 'isimulate-ie', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'isimulate_scripts' );
//Add Image Size for post thumb
add_image_size( 'post-thumb', 270, 152, array('center','center') );

//Add Image Size for home page product image
add_image_size( 'product-thumb', 295, 196, array('center','center') );

//Add Image Size for home page product logo
add_image_size( 'product-logo-thumb', 107, 41, array('center','center') );

//Add Image Size for waveforms thumb
add_image_size( 'product-logo', 132, 51, array('center','center') );

//Add Image Size for waveforms thumb
add_image_size( 'product-banner-image', 745, 378, array('center','center') );

//Add Image Size for waveforms thumb
add_image_size( 'waveforms-thumb', 266, 192, array('center','center') );
//Add Image Size for waveforms thumb
add_image_size( 'video-thumb', 338, 190, array('center','center') );
//Add Image Size for your-story
add_image_size( 'your-story-thumb', 380, 105, array('center','center') );
/**
 * Add Favicon
 *
 * Handles to add favicon
 *
 * @since Centre Stage 1.0
 **/
function isimulate_add_favicon() {
	echo '<link rel="shortcut icon" href="' . ISIMULATE_THEME_URL . '/favicon.ico' . '" />';
}
add_action( 'login_head',	'isimulate_add_favicon' );
add_action( 'admin_head',	'isimulate_add_favicon' ); 
add_action( 'wp_head',		'isimulate_add_favicon' );
if( ! function_exists('isimulate_login_customize') ) :
/**
 * Override Login Page Styles
 *
 * Handles to login logo
 *
 * @since iSimulate 1.0
 **/
function isimulate_login_customize() { ?>

    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo ISIMULATE_THEME_URL;?>/images/wp-logo.png);
			background-size:100%;
			-webkit-background-size:100%;
			-moz-background-size:100%;
			-ms-background-size:100%;
			width:234px;
			height:134px;
        }
		#loginform #wp-submit {
			background-color: #d71b1e;
			border:none;
			border-radius:3px;
			color: #FFF;
			display: inline-block;
			font-size: 16px;
			font-weight:600;
			padding: 5px 15px 5px 15px;
			text-decoration: none;
            text-transform:uppercase;
			text-shadow: none;
			height:auto;
			line-height:normal;
			box-shadow:none;
		}
		#loginform #wp-submit:hover{
			 background-color: #BA1619;
			 color: #fff;
			 text-decoration: none;
		}
		.login form .forgetmenot label { padding-top:10px; }
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'isimulate_login_customize' );
endif;
if( !function_exists('isimulate_login_logo_url') ) :
/**
 * Change Login Logo URL
 *
 * Handles to change login logo URL
 *
 * @since iSimulate 1.0
 **/
function isimulate_login_logo_url($url) {

   return home_url('/');
}
add_filter( 'login_headerurl', 'isimulate_login_logo_url' );
endif;

if( !function_exists('isimulate_wpcf7_ajax_loader') ) :
/**
 * Contact Form 7 Loader
 * 
 * Handles to change contact form 7 loader
 *
 * @since iSimulate 1.0
 **/
function isimulate_wpcf7_ajax_loader() {
	//Return New Loader
	return ISIMULATE_THEME_URL . '/images/ajax-loader.gif';
}
add_filter( 'wpcf7_ajax_loader', 'isimulate_wpcf7_ajax_loader' );
endif;
/*
if( !function_exists('isimulate_google_traslate_js')):
/**
 * Adding google traslate jquery
 *
 * @since iSimulate 1.0
 **/
/*
function isimulate_google_translate_js(){?>
	<script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
	</script>
	<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<?php }
add_action( 'wp_footer', 'isimulate_google_translate_js' );
endif;
*/
function isimulate_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyA5akwCTi2aaY_Z_hRnFviCc08mrylMnpk';
	return $api;
}
add_filter('acf/fields/google_map/api', 'isimulate_acf_google_map_api');

/**
 * Adding extra class to body tag
 *
 * @since iSimulate 1.0
 */
// add category nicenames in body and post class
function page_body_class( $classes ) {
    if( is_page_template( 'page-templates/template-coming-soon-product.php' )  ) :
	$classes[ ] = 'realitiproduct-page'; //Add extra class to team template 
    endif;
    return $classes;
}
add_filter( 'body_class', 'page_body_class' );

/**
 * Custom template tags for this theme.
 *
 * @since iSimulate 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

//Theme shortcode
require get_template_directory() . '/inc/theme-shortcodes.php';

//Theme ajax
require get_template_directory() . '/inc/theme-ajax.php';

//Custom Post Type For yourstory
require ISIMULATE_THEME_DIR . '/inc/custom-posts/your-story.php';
//Custom Post Type For product
require ISIMULATE_THEME_DIR . '/inc/custom-posts/products.php';

<?php
/**
 * Thumbnail Option template
 *
 * Override this template by copying it to 'yourtheme/woocommerce/single-product/js/options-thumbnails.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 3.7.0
 * @since   3.7.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?><script type="text/template" id="tmpl-wc_cp_options_thumbnails">
	<# if ( data.length > 0 ) { #>
		<ul class="component_option_thumbnails_container cp_clearfix" style="list-style:none">
			<# for ( var index = 0; index <= data.length - 1; index++ ) { #>
				<li id="component_option_thumbnail_container_{{ data[ index ].option_id }}" class="component_option_thumbnail_container {{ data[ index ].outer_classes }}">
					<div id="component_option_thumbnail_{{ data[ index ].option_id }}" class="option_selection component_option_thumbnail cp_clearfix {{ data[ index ].inner_classes }}" data-name="{{ data[ index ].option_display_title }}" data-price="{{ data[ index ].option_price_html }}" data-val="{{ data[ index ].option_id }}">
						<a class="component_option_thumbnail_tap" href="#" ></a>
						<div class="image thumbnail_image">
							{{{ data[ index ].option_thumbnail_html }}}
						</div>
						<div class="checkboxCircle">
						<img src="/wp-content/uploads/2017/10/icon-checkmark.png">
						</div>
						<div class="thumbnail_description">
							<span class="thumbnail_title title">{{{ data[ index ].option_display_title }}} - </span>
							<# if ( data[ index ].option_price_html ) { #>
								<span class="thumbnail_price price">{{{ data[ index ].option_price_html }}}</span>
							<# } #>
						</div>
					</div>
				</li>
			<# } #>
		</ul>
		<div class="license-input license-upgrade hidden"><label>License Upgrade: </label><input type="text" id="license" class="input-text text license" name="license" title="License" placeholder="Enter your current license number"></div>
		<div class="license-input license-conversion hidden"><label>License Convertion: </label><input type="text" id="conversion" class="input-text text license" name="conversion" title="Conversion" placeholder="Enter your current license number for trading"></div>

	<# } #>
	<# if ( data.length === 0 ) { #>
		<p class="results_message no_query_results">
			<?php _e( 'No results found.', 'woocommerce-composite-products' ); ?>
		</p>
	<# } else if ( _.where( data, { is_hidden: false } ).length === 0 ) { #>
		<p class="results_message no_compat_results">
			<?php _e( 'No compatible options to display.', 'woocommerce-composite-products' ); ?>
		</p>
	<# } #>
</script>

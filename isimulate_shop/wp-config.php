<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'isimulate_shop');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b<W_*!oA5EiTw-pfUz2v;;9gven&T%UWsT*0YAK AQ>^<v<-+vK.4kTGE?WzC0<=');
define('SECURE_AUTH_KEY',  'Wsw/w0tbR*KlZLseZEpc_9Sa^9csuVW$e!JyY7>;SyE5kml2Euo[Jf[@_1=z[>OV');
define('LOGGED_IN_KEY',    '=o|8:<DSHJ-}cr4:gv[MIe@{miPZmSx@WaRiMTp^h5WYeVClxx~YG8f,<df4lTn>');
define('NONCE_KEY',        'vivR{11mE97idh[Xp]g?iuP)D~dPB{GA<Chvf}Td/XSW_QSwrMEWFUurYjeix;]R');
define('AUTH_SALT',        '?<_(pp*wikD_z3NKV7_@qkT2?t3DAU(0eyNIG9k{I:(evD{O)*(DN^tz-VIlOKmC');
define('SECURE_AUTH_SALT', '3$yoG K--dxpcc&{wey&$$-SA;YM.n+47*F<9o7XC[=DQwR7&7|L6qKILwt#R+=(');
define('LOGGED_IN_SALT',   '^7ZmNOIvVzzF@U/cr3.rW9dHc;1r}zgZ0EGEo?{~d*yz`-Eu;Z;syv8Zcah(ZtPD');
define('NONCE_SALT',       'GUF,%)NU.^RG4kmq$b.BXI#kI*pih} S&7C)DC-sKKWMi88NycmIqD;;C7`PglAq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
